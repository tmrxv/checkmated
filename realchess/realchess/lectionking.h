#pragma once
#include "Lection.h"
class LectionKing :
	public Lection
{
private:
	vector<vector<Vector2i>> positions;
public:
	LectionKing();
	LectionKing(string);
	virtual ~LectionKing();
	virtual void event_handling(Board*, int);
};

