#include "king.h"

King::King(bool is_white)
{
	type = KING;
	this->is_white = is_white;
	max_steps = 1;
	moved_already = false;
	checked = false;
}


King::~King()
{

}

vector<Vector2i> King::get_possible_destinations(Vector2i my, boardContent& content)
{
	vector<Vector2i> possible_destinations;
	vector<Vector2i> help;

	for (int x = -1; x <= 1; x++)
	{
		for (int y = -1; y<=1; y++)
		{
			if (!(x==0 && y==0))
			{
				help = get_linear_destinations(my, Vector2i(x, y), content);
				if (!help.empty())
				{
					for (auto d : help)
					{
						possible_destinations.push_back(d);
					}
					help.clear();
				}
			}
		}
	}
	get_possible_destinations_for_rochade(my, content, possible_destinations);

	return possible_destinations;
}

void King::get_possible_destinations_for_rochade(Vector2i my, boardContent content, vector<Vector2i>& possible_destinations)
{
	//rochade
	if (!moved_already && !checked)
	{
		int pos_of_castle_x = 0, pos_of_field_x = 0, pos_of_extra_field_x;
		for (int x = -2; x <= 2; x += 2)
		{
			if (content[my.y][my.x + x] == NULL)    //no barriers betwwn king and castle
			{
				//get positions of castle
				if (x == -2)    //left rochade
				{
					pos_of_castle_x = x - 2;
					pos_of_field_x = x + 1;
					pos_of_extra_field_x = x - 1;
				}
				else              //right rochade
				{
					pos_of_castle_x = x + 1;
					pos_of_field_x = x - 1;
					pos_of_extra_field_x = pos_of_field_x;
				}

				if (content[my.y][my.x + pos_of_field_x] == NULL && content[my.y][my.x + pos_of_extra_field_x] == NULL)
				{
					if (content[my.y][my.x + pos_of_castle_x] != NULL)
					{
						if (content[my.y][my.x + pos_of_castle_x]->get_type() == CASTLE)
						{
							if (!((Castle*)content[my.y][my.x + pos_of_castle_x])->get_moved_already())
							{
								possible_destinations.push_back(Vector2i(my.x + x, my.y));
							}
						}
					}
				}
			}
		}
	}
}

void King::figure_already_moved()
{
	moved_already = true;
}

bool King::get_moved_already()
{
	return moved_already;
}

void King::set_checked(bool value)
{
	checked = value;
}