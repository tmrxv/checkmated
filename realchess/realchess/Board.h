#pragma once
#include <SFML/Graphics.hpp>
#include "figure.h"
#include "castle.h"
#include "knight.h"
#include "bishop.h"
#include "queen.h"
#include "king.h"
#include "pawn.h"
#include "rectangleshape.h"
#include "text.h"
#include "component.h"
#include "notator.h"

using namespace std;
using namespace sf;

class Board;
enum StringType
{
	LEFT, RIGHT, PROMOTION, CHECKMATED, CHECKANDCHECKMATED, REMIS, WHITEWINS, BLACKWINS
};

class Board : public Component
{
private:
	boardContent content;
	Notator notator;
	array<std::string, 9> notator_strings;
	vector<Texture*> textures;
	vector<string> notation;
	bool white_turn = true;
	int moving_speed = 15;
	Vector2i selected_figure_coordinates;
	vector<Vector2i> destinations;
	gui::RectangleShape blueTile;
	gui::RectangleShape previous_field;
	gui::RectangleShape current_field;
	gui::RectangleShape possible_field;
	gui::RectangleShape rect_check;
	const Color brown = Color(216, 147, 0, 255);
	vector<Vector2i> castling_destinations;
	vector<gui::RectangleShape> possible_fields;

	//booleans for notator
	bool enemy_on_field = false;
	bool long_castling = false;
	bool short_castling = false;
	bool promotion = false;
	bool king_checked = false;
	bool remis = false;
	bool check_mated = false;
	bool en_passant = false;
	bool end = false;
	bool new_notation = false;

	//textures
	Texture* txt_king = new Texture();
	Texture* txt_queen = new Texture();
	Texture* txt_knight = new Texture();
	Texture* txt_castle = new Texture();
	Texture* txt_bishop = new Texture();
	Texture* txt_pawn = new Texture();

	Figure* clicked_figure = NULL;
	Figure* selected_figure = NULL;
	Pawn* pawn_to_remember = NULL;

	//const
	Font font;
	const Color color = Color(40, 40, 40, 255);
	const float field_size = 81;
	const Vector2f offset_figure = Vector2f(36, 36);

	//functions
	void load_sprites();
	void finish_turn();
	void move_figure(Vector2i);
	void castling(Vector2i);
	void pawn_promotion(Vector2i);
	void after_move_checks(Vector2i);
	void check_move(Vector2i, Figure*, Vector2i, vector<Vector2i>&);
	void delete_impossible_destinations(Vector2i, Vector2i, Vector2i, Figure*, vector<Vector2i>&);
	void check_en_passant(Vector2i, Figure*, Vector2i, vector<Vector2i>&);
	void notate(Vector2i, Figure*, Vector2i, boardContent&);
	bool check_if_king_can_move(int x, int y, vector<Vector2i>&);
	void correct_destinations(vector<Vector2i>&);
	vector<Vector2i> get_destinations_of_all_figures(bool, bool);

public:
	//con-/destructor
	Board();
	virtual ~Board();
	//functions
	boardContent& get_content();
	bool new_notation_available();
	vector<string>* get_notation();
	void set_content(boardContent);
	void event_handling(int, Vector2f);
	void event_handling(int, Vector2i);
	void drawMe(RenderWindow& window);
	void updateMe();
	void reset_game();
	void fadeInFigures();
	void fadeOutFigures();
	void set_textures();
#ifdef TEST
	friend class UnitTest::UnitTest1;
#endif
};

