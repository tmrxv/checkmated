#pragma once
#include "lection.h"
#include "lectionPawn.h"
#include "lectionBishop.h"
#include "lectionKnight.h"
#include "lectionCastle.h"
#include "lectionQueen.h"
#include "lectionKing.h"
#include "strategy.h"
#include "text.h"
#include <vector>

class Trainer
{
private:
	Font font;
	LectionPawn* lectionPawn;
	LectionBishop* lectionBishop;
	LectionKnight* lectionKnight;
	LectionCastle* lectionCastle;
	LectionQueen* lectionQueen;
	LectionKing* lectionKing;
	Strategy* strategy1;


	vector<Lection*> lections;
	vector<Lection*> strategies;
	vector<gui::Text> lectionNames;
	vector<gui::Text> strategyNames;
	Lection* selectedLection = NULL;

public:
	Trainer();
	virtual ~Trainer();
	vector<Lection*> get_lections();
	vector<Lection*> get_strategies();
	vector<gui::Text> get_lection_names();
	vector<gui::Text> get_strategy_names();
	void set_selected_lection(Lection*);
	Lection* get_selected_lection();
};