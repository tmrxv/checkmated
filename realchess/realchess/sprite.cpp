#include "sprite.h"

gui::Sprite::Sprite()
{
	spr = sf::Sprite();
}

gui::Sprite::Sprite(string texturePath)
{
	Texture* texture = new Texture();
	texture->loadFromFile(texturePath);
	setTexture(*texture, true);
}

gui::Sprite::Sprite(string texturePath, Vector2f position)
{
	Texture* texture = new Texture();
	texture->loadFromFile(texturePath);
	setTexture(*texture, true);
	setPosition(position);
}

gui::Sprite::~Sprite()
{
}

void gui::Sprite::setAlphas()
{
	sf::Color col = spr.getColor();
	col.a = alpha;
	spr.setColor(col);
}

void gui::Sprite::setScale(Vector2f scale)
{
	spr.setScale(scale);
}

void gui::Sprite::setTexture(Texture& texture, bool resetRect)
{
	spr.setTexture(texture, resetRect);
}

void gui::Sprite::setColor(Color color)
{
	alpha = color.a;
	spr.setColor(color);
}


IntRect gui::Sprite::getTextureRect()
{
	return spr.getTextureRect();
}

Color gui::Sprite::getColor()
{
	return spr.getColor();
}

void gui::Sprite::draw(RenderWindow& window)
{
	draw(window, Vector2f(0, 0));
}

void gui::Sprite::draw(RenderWindow& window, Vector2f offset)
{
	spr.setPosition(position + offset);
	window.draw(spr);
	spr.setPosition(position);
}

FloatRect gui::Sprite::getLocalBounds()
{
	return spr.getLocalBounds();
}

FloatRect gui::Sprite::getGlobalBounds()
{
	return spr.getGlobalBounds();
}

void gui::Sprite::update()
{
	do_fading();
	do_moving();
}

const Texture* gui::Sprite::getTexture()
{
	return spr.getTexture();
}
