#pragma once
#include "scene.h"
#include "menu.h"
#include "rectangleshape.h"
#include "menuentry.h"

class SceneMainMenu : public Scene
{
private:
	gui::Sprite pane_menu;
	gui::Sprite pane_options;
	gui::Sprite logo;
	gui::Sprite arrowLeft;
	gui::Sprite arrowRight;
	gui::Sprite* infobox;
	gui::Sprite* pressedButton;
	gui::Text menuname;
	gui::Text credits;
	gui::Text* infotext;
	gui::Text* infotext_shadow;
	Font font;
	Font font2;
	FloatRect buttonArrowLeft;
	FloatRect buttonArrowRight;

	Component comp_infobox;
	Container cont_entries;

	vector<Menu*> menus;
	Menu* menuChess;
	Menu* menuTraining;
	Menu* menuAnalysis;
	Menu* menuLectionSelection;
	int currentMenu;
	int nextMenu;

	bool infoboxVisible = false;
	bool inputAllowed = true;
	bool leftButtonClicked = false;
	const Vector2f infoBoxPosition = Vector2f(520, 635);
	const Vector2f infoBoxStartPosition = Vector2f(520, 720);
	const Vector2f menunameOrigin = Vector2f(951, 84);
	const Vector2f infoboxOrigin = Vector2f(338, 36);
	const Vector2f positionArrowLeft = Vector2f(657, 47);
	const Vector2f positionArrowRight = Vector2f(1173, 47);
public:
	SceneMainMenu();
	SceneMainMenu(GUI*, int, int);
	virtual ~SceneMainMenu();

	//functions
	void draw(RenderWindow& window);
	void event_handling(int, Vector2f);
	void resetMenuName();
	void startup();
	void shutdown();
};