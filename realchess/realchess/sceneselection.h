#pragma once
#include "scene.h"
#include "container.h"
#include "label.h"
#include "Board.h"
#include "text.h"
#include "gui.h"

class GUI;
class Trainer;
class SceneSelection : public Scene
{
private:
	Board* board;
	Trainer* trainer;
	vector<Lection*> lections;
	gui::RectangleShape rectLection;
	FloatRect selectButton;
	vector<FloatRect> selectButtons;
	vector<gui::RectangleShape> lectionFields;
	vector<gui::Text> lectionNames;
	Lection* selectedLection = NULL;
	Font font;
public:
	SceneSelection();
	SceneSelection(GUI*, Board*, Trainer*);
	virtual ~SceneSelection();

	//functions
	void draw(RenderWindow& window);
	void event_handling(int, Vector2f);
	void startup();
	void shutdown();
};