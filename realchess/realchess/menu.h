#pragma once
#include "menuentry.h"
#include "container.h"

class Menu : public Container
{
private:
	string name;
	Color color;
	Font font;
	bool inputAllowed = true;
	MenuEntry* selectedEntry = NULL;
	int numberOfEntries;
	const Vector2f positionFirstEntry = Vector2f(655, 180);
	const Vector2f offsetEntry = Vector2f(-31, 115);
	bool changeScene = false;

	Component selection;
	vector<MenuEntry*> entries;
	vector<FloatRect> buttons;

public:
	Menu();
	Menu(string, Font&, Color);
	virtual ~Menu();

	//functions
	MenuEntry* getSelectedEntry();
	string getName();
	bool sceneChanging();
	void fadeInEntries(int);
	void fadeOutEntries(int);
	void unselect();
	void drawMe(RenderWindow&);
	void push_back_entry(MenuEntry*);
	Color getColor();
	void eventHandling(int, Vector2f);
};