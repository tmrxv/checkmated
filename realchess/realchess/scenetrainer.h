#pragma once
#include "scene.h"
#include "container.h"
#include "label.h"
#include "Board.h"
#include "text.h"
#include "gui.h"

class GUI;
class Trainer;
class SceneTraining : public Scene
{
private:
	Board* board;
	Trainer* trainer;
	vector<gui::Text> lectionInfos;
	Lection* selectedLection = NULL;
	int textLocation = 0;
	bool boardIsInteractable = false;
public:
	SceneTraining();
	SceneTraining(GUI*, Board*, Trainer*);
	virtual ~SceneTraining();

	//functions
	void draw(RenderWindow& window);
	void event_handling(int, Vector2f);
	void startup();
	void fixString(int, int, int);
	void shutdown();
};
