#include "scenetrainer.h"
#include <iostream>

SceneTraining::SceneTraining()
{
}

SceneTraining::SceneTraining(GUI* gui, Board* board, Trainer* trainer) : Scene(gui)
{
	this->board = board;
	this->trainer = trainer;
	selectedLection = this->trainer->get_selected_lection();
	lectionInfos = selectedLection->get_lection_infos();
	startup();
}

SceneTraining::~SceneTraining()
{

}

void SceneTraining::startup()
{
	gui->showBoard();
	int offset = 40;
	int stringLength;

	for (int i = 0; i < lectionInfos.size(); i++)
	{
		stringLength = (int)lectionInfos.at(i).getString().size();
		for (int j = 50; j < stringLength; j += 50)
		{
			if (stringLength > j)
			{
				fixString(i, stringLength, j);
			}
			else
			{
				break;
			}
		}

		if (i != 0)
		{
			lectionInfos.at(i).setPosition(Vector2f(730, lectionInfos.at(i - 1).getLocalBounds().height
				+ lectionInfos.at(i - 1).getPosition().y + (float)offset));
		}
		else
		{
			lectionInfos.at(i).setPosition(Vector2f(730, (float)offset));
		}
		lectionInfos.at(i).setAlpha(0);
	}
}

void SceneTraining::shutdown()
{

}

void SceneTraining::event_handling(int left_mouse_click, Vector2f m_coordinates_fields)
{

	if (state == STARTING)
	{
		if (board->is_triggered())
		{
			state = RUNNING;
		}
	}
	else if (!boardIsInteractable)
	{
		selectedLection->event_handling(board, left_mouse_click);
	}

	if (selectedLection->boardIsInteractable())
	{
		boardIsInteractable = true;
		board->reset_game();
		gui->setBoardInteractable(true);
	}

	//fade in text in correct order
	if (selectedLection->showInformation())
	{
		lectionInfos.at(textLocation).fade_in(20);
		if (textLocation < lectionInfos.size() - 1)
		{
			textLocation++;
		}
	}

	for (int i = 0; i < lectionInfos.size(); i++)
	{
		lectionInfos.at(i).update();
	}
}

void SceneTraining::draw(RenderWindow& window)
{
	if (!lectionInfos.empty())
	{
		for (int i = 0; i < lectionInfos.size(); i++)
		{
			lectionInfos.at(i).draw(window);
		}
	}
}

void SceneTraining::fixString(int i, int length, int position)
{
	string fixedStr, str, str2;
	string key = " ";
	for (auto pos = position; pos < length; pos++)
	{
		if (lectionInfos.at(i).getString().at(pos) == key)
		{
			str = lectionInfos.at(i).getString().substr(0, pos + 1); //+1 because key must be part of str
			str2 = lectionInfos.at(i).getString().substr(pos + 1);
			fixedStr = str + "\n" + str2;
			lectionInfos.at(i).setString(fixedStr);
			break;
		}
	}
}