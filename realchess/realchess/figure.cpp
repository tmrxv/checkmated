#include "figure.h"

Figure::Figure()
{

}

Figure::~Figure()
{

}

bool Figure::check_if_white()
{
	return is_white;
}

FigureType Figure::get_type()
{
	return type;
}

bool Figure::in_bounds(int y, int x)
{
	if (x < 0 || x > 7)
	{
		return false;
	}
	if (y < 0 || y > 7)
	{
		return false;
	}
	return true;
}