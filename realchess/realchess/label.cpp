#include "label.h"
#include <iostream>

Label::Label()
{

}

Label::Label(Color color_bounds, Color color_text, Vector2f position, Vector2f size, string text, Font& font, int alignment, bool upside_down)
{
	this->position = position;
	this->destination_position = position;

	bounds = new gui::RectangleShape();
	bounds->setFillColor(color_bounds);
	bounds->setSize(size);

	this->text = new gui::Text();
	this->text->setFillColor(color_text);
	this->text->setFont(font);
	this->text->setString(text);
	this->text->setCharacterSize(50);
	this->alignment = alignment;
	if (upside_down)
	{
		this->upside_down = upside_down;
		this->text->setRotation(180);
	}

	parts.push_back(bounds);
	parts.push_back(this->text);
	set_text_alignment();
}

Label::~Label()
{
}

void Label::set_text_alignment()
{
	float offsetY = (bounds->getLocalBounds().height - text->getLocalBounds().height) / 2;
	float offsetX;

	//left
	if (alignment == -1)
	{
		offsetX = 20;
	}
	//center
	else if (alignment == 0)
	{
		offsetX = (bounds->getSize().x / 2) - (text->getLocalBounds().width / 2);
	}
	//right
	else if (alignment == 1)
	{
		offsetX = (bounds->getSize().x) - (text->getLocalBounds().width) - 20;
	}
	if (this->upside_down)
	{
		text->setPosition(Vector2f(bounds->getSize().x - offsetX, bounds->getSize().y - offsetY));
	}
	else
	{
		text->setPosition(Vector2f(offsetX - text->getGlobalBounds().left, offsetY - text->getGlobalBounds().top));
	}
}

void Label::set_string(string text)
{
	this->text->setString(text);
}

string Label::get_string()
{
	return text->getString();
}

void Label::event_handling(int, Vector2i)
{

}