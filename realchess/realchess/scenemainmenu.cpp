#include "scenemainmenu.h"
#include <iostream>
#include "gui.h"

SceneMainMenu::SceneMainMenu()
{
}

SceneMainMenu::SceneMainMenu(GUI* gui, int activeMenu, int selectedEntry) : Scene(gui)
{
	startup();

	//colors
	Color color_menu_chess = Color(40, 40, 40, 255);

	//font used in main menu
	font.loadFromFile("C:/Windows/Fonts/calibrib.ttf");
	font2.loadFromFile("C:/Windows/Fonts/calibrib.ttf");

	//background

	//pane menu
	pane_menu = gui::Sprite("images/gui/main_menu/pane_menu.png", Vector2f(481, 152));
	pane_menu.setAlpha(185);

	//pane options
	pane_options = gui::Sprite("images/gui/main_menu/pane_options.png", Vector2f(1140, 152));

	//infobox
	infobox = new gui::Sprite("images/gui/main_menu/infobox.png");

	//infotext
	infotext = new gui::Text(font2, Color::White, 35);
	infotext_shadow = new gui::Text(font2, Color(115, 115, 115, 255), 35);

	comp_infobox.push_back(infobox);
	comp_infobox.push_back(infotext_shadow);
	comp_infobox.push_back(infotext);
	comp_infobox.setPosition(infoBoxStartPosition);

	//menu name
	menuname = gui::Text(font, Color::White, 84, menunameOrigin);
	menuname.setStyle(Text::Underlined | Text::Italic);

	//arrow buttons
	arrowLeft = gui::Sprite("images/gui/main_menu/arrow_left.png", positionArrowLeft);
	arrowRight = gui::Sprite("images/gui/main_menu/arrow_right.png", positionArrowRight);
	buttonArrowLeft = FloatRect(arrowLeft.getPosition() - Vector2f(-10, -10), Vector2f(90, 90));
	buttonArrowRight = FloatRect(arrowRight.getPosition() - Vector2f(-10, -10), Vector2f(90, 90));

	//logo
	logo = gui::Sprite("images/gui/main_menu/logo.png", Vector2f(79, 156));

	//credits
	credits = gui::Text(font2, Color::White, 20);
	credits.setString("by Stefan Cvjetkovic & Tamir Bukhshandas");
	credits.setPosition(Vector2f(10, 690));
	credits.setAlpha(185);

	//menus
	menuChess = new Menu("Schachspiel", font, color_menu_chess);
	menuTraining = new Menu("Training", font, Color(225, 0, 0));
	menuAnalysis = new Menu("Analyse", font, Color(0, 40, 227));

	menuChess->push_back(&pane_menu);
	menuTraining->push_back(&pane_menu);
	menuAnalysis->push_back(&pane_menu);

	//entries
	MenuEntry* chess = new MenuEntry("Schach", font, "icon_entry_chess");
	chess->setInformation("Standardschach: Spieler vs. Spieler");
	chess->setScene(CHESS);
	MenuEntry* stockfish = new MenuEntry("Stockfish", font, "icon_entry_stockfish");
	stockfish->setInformation("Schachpartie gegen den Computer");
	MenuEntry* speedchess = new MenuEntry("Blitzschach", font, "icon_entry_speedchess");
	speedchess->setInformation("Schach mit begrenzter Bedenkzeit");
	MenuEntry* horde = new MenuEntry("Horde", font, "icon_entry_horde");
	horde->setInformation("Schlage ein Horde an Bauern");


	MenuEntry* lectionSelection = new MenuEntry("Lektion", font, "icon_entry_chess");
	MenuEntry* strategySelection = new MenuEntry("Strategien", font, "icon_entry_chess");
	lectionSelection->setInformation("Ja man");
	strategySelection->setInformation("yes man");
	lectionSelection->setScene(SELECTION);
	strategySelection->setScene(SELECTION);

	MenuEntry* anlysis = new MenuEntry("Analysieren", font, "icon_entry_chess");
	anlysis->setInformation("Gib eam");

	menuChess->push_back_entry(chess);
	menuChess->push_back_entry(speedchess);
	menuChess->push_back_entry(horde);
	menuChess->push_back_entry(stockfish);
	menuTraining->push_back_entry(lectionSelection);
	menuTraining->push_back_entry(strategySelection);
	menuAnalysis->push_back_entry(anlysis);

	//menu vector
	menus.push_back(menuChess);
	menus.push_back(menuTraining);
	menus.push_back(menuAnalysis);

	//set active menu
	currentMenu = activeMenu;
	gui->changeColor(menus.at(currentMenu)->getColor());
	resetMenuName();
}

SceneMainMenu::~SceneMainMenu()
{
}

void SceneMainMenu::draw(RenderWindow& window)
{
	logo.draw(window);
	menuname.draw(window);
	arrowLeft.draw(window);
	arrowRight.draw(window);
	menus.at(currentMenu)->drawMe(window);
	comp_infobox.draw(window);
	pane_options.draw(window);
	credits.draw(window);
}

void SceneMainMenu::event_handling(int mouse_leftClick, Vector2f mouse_coordinates)
{
	if (menus.at(currentMenu)->sceneChanging() && state == RUNNING)
	{
		shutdown();
	}
	else if (state == RUNNING)
	{
		menus.at(currentMenu)->eventHandling(mouse_leftClick, mouse_coordinates);
		if (mouse_leftClick == 1)
		{
			if (inputAllowed)
			{
				//left arrow
				if (buttonArrowLeft.contains((Vector2f)mouse_coordinates))
				{
					menuname.move_to(menunameOrigin - Vector2f(40, 0), 30, SLOWDOWN);
					menuname.fade_out(30);
					inputAllowed = false;
					leftButtonClicked = true;
					nextMenu = (currentMenu > 0) ? currentMenu - 1 : (int)(menus.size() - 1);
					comp_infobox.move_to(infoBoxStartPosition, 18, SLOWDOWN);
					infoboxVisible = false;
					menus.at(currentMenu)->fadeOutEntries(30);
					gui->changeColor(menus.at(nextMenu)->getColor());
				}
				//right arrow
				else if (buttonArrowRight.contains((Vector2f)mouse_coordinates))
				{
					menuname.move_to(menunameOrigin + Vector2f(40, 0), 30, SLOWDOWN);
					menuname.fade_out(30);
					inputAllowed = false;
					leftButtonClicked = false;
					nextMenu = (currentMenu == menus.size() - 1) ? 0 : currentMenu + 1;
					comp_infobox.move_to(infoBoxStartPosition, 18, SLOWDOWN);
					infoboxVisible = false;
					menus.at(currentMenu)->fadeOutEntries(30);
					gui->changeColor(menus.at(nextMenu)->getColor());
				}
			}
		}
		//if menuname faded out completely
		if (menuname.is_triggered() && !inputAllowed)
		{
			if (leftButtonClicked)
			{
				menuname.setPosition(menunameOrigin + Vector2f(40, 0));
				menuname.move_by(Vector2f(-40, 0), 30, SLOWDOWN);
			}
			else
			{
				menuname.setPosition(menunameOrigin - Vector2f(40, 0));
				menuname.move_by(Vector2f(40, 0), 30, SLOWDOWN);
			}
			menus.at(currentMenu)->unselect();
			currentMenu = nextMenu;
			menus.at(currentMenu)->fadeInEntries(30);
			menuname.fade_in(30);
			resetMenuName();
			inputAllowed = true;
		}

		//check if/what entry is selected
		if (menus.at(currentMenu)->getSelectedEntry() != NULL && inputAllowed)
		{
			//make infobox visible
			if (!infoboxVisible)
			{
				comp_infobox.move_to(infoBoxPosition, 18, SLOWDOWN);
				infoboxVisible = true;
			}
			//set strings
			infotext->setString(menus.at(currentMenu)->getSelectedEntry()->getInformation());
			infotext_shadow->setString(menus.at(currentMenu)->getSelectedEntry()->getInformation());
			//new text origin
			Vector2f textPosition = Vector2f(infotext->getLocalBounds().width / 2,
				infotext->getLocalBounds().height / 2)
				+ Vector2f(infotext->getLocalBounds().left, infotext->getLocalBounds().top);
			infotext->setOrigin(textPosition);
			infotext_shadow->setOrigin(textPosition);
			//place text into the center of infobox
			infotext->setPosition(infoboxOrigin);
			infotext_shadow->setPosition(infoboxOrigin + Vector2f(2, 2));
		}
	}
	else if (state == CLOSING)
	{
		if (logo.is_triggered())
		{
			gui->changeScene(menus.at(currentMenu)->getSelectedEntry()->getScene());
			return;
		}
	}
	menus.at(currentMenu)->update();
	comp_infobox.update();
	arrowLeft.update();
	arrowRight.update();
	menuname.update();
	pane_options.update();
	logo.update();
	credits.update();
}

void SceneMainMenu::resetMenuName()
{
	//set menu name
	menuname.setString(menus.at(currentMenu)->getName());
	Vector2f origin = Vector2f(menuname.getLocalBounds().width / 2,
		menuname.getLocalBounds().height / 2)
		+ Vector2f(menuname.getLocalBounds().left, menuname.getLocalBounds().top);
	menuname.setOrigin(origin);
}

void SceneMainMenu::startup()
{
	state = RUNNING;
}

void SceneMainMenu::shutdown()
{
	state = CLOSING;
	logo.move_by(Vector2f(-700, 0), 4, ACCELERATE);
	menus.at(currentMenu)->move_by(Vector2f(800, 0), 4, ACCELERATE);
	pane_options.move_by(Vector2f(800, 0), 4, ACCELERATE);
	comp_infobox.move_by(Vector2f(800, 0), 4, ACCELERATE);
	arrowLeft.fade_out(10);
	arrowRight.fade_out(10);
	menuname.fade_out(10);
	credits.fade_out(10);
}