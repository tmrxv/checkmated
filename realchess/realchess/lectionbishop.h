#pragma once
#include "Lection.h"
class LectionBishop :
	public Lection
{
private:
	vector<vector<Vector2i>> positions;
public:
	LectionBishop();
	LectionBishop(string);
	virtual ~LectionBishop();
	virtual void event_handling(Board*, int);
};

