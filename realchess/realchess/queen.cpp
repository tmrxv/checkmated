#include "queen.h"

Queen::Queen(bool is_white)
{
	type = QUEEN;
	this->is_white = is_white;
}

Queen::~Queen()
{

}

vector<Vector2i> Queen::get_possible_destinations(Vector2i my, boardContent& content)
{
	vector<Vector2i> possible_destinations;
	vector<Vector2i> help;

	for (int x = -1; x <= 1; x++)
	{
		for (int y = -1; y <= 1; y++)
		{
			if (!(x == 0 && y == 0))
			{
				help = get_linear_destinations(my, Vector2i(x, y), content);
				if (!help.empty())
				{
					for (auto d : help)
					{
						possible_destinations.push_back(d);
					}
					help.clear();
				}
			}
		}
	}
	return possible_destinations;
}