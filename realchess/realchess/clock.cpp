#include "clock.h"
#include <iostream>

my::Clock::Clock()
{
}

my::Clock::Clock(int startTime_s, bool countUpwards)
{
	stored_time = (sf::Int64)(startTime_s * 1000000);
	this->countUpwards = countUpwards;
	
	sf::Int64 tmp = stored_time;

	hours = tmp / 3600000000;
	tmp = tmp - hours * 3600000000;
	cout << hours << endl;

	minutes = tmp / 60000000;
	tmp = tmp - minutes * 60000000;
	cout << minutes << endl;

	seconds = tmp / 1000000;
	tmp = tmp - seconds * 1000000;
	cout << seconds << endl;
}

my::Clock::~Clock()
{
}

void my::Clock::update()
{
	if (!paused)
	{
		switch (countUpwards)
		{
			case true:
			{
				if ((stored_time + clock.getElapsedTime().asMicroseconds()) > seconds * 1000000)
				{
					seconds++;
					if (seconds == 60)
					{
						seconds = 0;
						minutes++;
					}
					if (minutes == 60)
					{
						minutes = 0;
						hours++;
					}
				}
				break;
			}

			case false:
			{
				if ((stored_time - clock.getElapsedTime().asMicroseconds()) < seconds)
				{
					seconds--;
					if (seconds < 0)
					{
						seconds = 59;
						minutes--;
					}
					if (minutes < 0)
					{
						minutes = 59;
						hours--;
					}
				}
				break;
			}
		}
	}
}

void my::Clock::pause()
{
	stored_time = clock.getElapsedTime().asMicroseconds();
	paused = true;
}

void my::Clock::resume()
{
	clock.restart();
	paused = false;
}

string my::Clock::getSeconds()
{
	if (seconds < 10)
	{
		return "0" + to_string(seconds);
	}
	else
	{
		return to_string(seconds);
	}
}

string my::Clock::getMinutes()
{
	if (minutes < 10)
	{
		return "0" + to_string(minutes);
	}
	else
	{
		return to_string(minutes);
	}
}

string my::Clock::getHours()
{
	if (hours < 10)
	{
		return "0" + to_string(hours);
	}
	else
	{
		return to_string(hours);
	}
}