#pragma once
#include <SFML/Graphics.hpp>

using namespace sf;
using namespace std;

enum MovementType
{
	ACCELERATE, SLOWDOWN
};

class Movable
{
protected:
	Vector2f position = Vector2f(0, 0);
	Vector2f destination_position = Vector2f(0, 0);
	Vector2f start_position = Vector2f(0, 0);
	float speed_movement = 1;
	bool trigger = false;
	bool wait_for_trigger = false;
	MovementType move_type = SLOWDOWN;

public:
	Movable();
	virtual ~Movable();

	//functions
	void do_moving();
	void move_to(Vector2f, float, MovementType);
	void move_by(Vector2f, float, MovementType);
	void setPosition(Vector2f);
	Vector2f getPosition();
	bool is_triggered();
};