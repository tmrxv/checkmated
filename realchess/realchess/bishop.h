#pragma once
#include "figureLinear.h"

class Bishop : public FigureLinear
{
public:
	//con-/destructor
	Bishop(bool);
	virtual ~Bishop();

	//methods
	vector<Vector2i> get_possible_destinations(Vector2i, boardContent&);
};