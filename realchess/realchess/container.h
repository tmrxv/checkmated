#pragma once
#include "component.h"
#include "sprite.h"
#include "rectangleshape.h"

class Container : public Movable
{
protected:
	vector<Component*> components;
	vector<GraphicalObject*> objects;

	//fumnctions
	void setAlphas();
public:
	Container();
	Container(Vector2f);
	virtual ~Container();

	//functions
	void draw(RenderWindow&);
	void draw(RenderWindow&, Vector2f);
	void update();
	void push_back(Component*);
	void push_back(GraphicalObject*);
	void push_back(vector<gui::Sprite*>);
	void push_back(vector<gui::RectangleShape*>);
};