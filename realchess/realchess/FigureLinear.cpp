#include "figureLinear.h"

FigureLinear::FigureLinear()
{

}


FigureLinear::~FigureLinear()
{

}

vector<Vector2i> FigureLinear::get_linear_destinations(Vector2i my, Vector2i direction, boardContent& board)
{
	int x = direction.x;
	int y = direction.y;

	bool goFurther = true;
	int steps = 1;
	vector<Vector2i> possible_destinations;

	do
	{
		//check if inbounds
		if (in_bounds(my.x + (x*steps), my.y + (y*steps)))
		{
			//check if empty
			if (board[my.y + (y*steps)][my.x + (x*steps)] == NULL)
			{
				possible_destinations.push_back(Vector2i(my.x + (x*steps), my.y + (y*steps)));
			}
			//check if enemy on field
			else if (board[my.y + (y*steps)][my.x + (x*steps)]->check_if_white() != this->is_white)
			{
				possible_destinations.push_back(Vector2i(my.x + (x*steps), my.y + (y*steps)));
				goFurther = false;
			}
			else //my figure
			{
				goFurther = false;
			}
		}
		else
		{
			goFurther = false;
		}

		if (steps < max_steps)
		{
			steps++;
		}
		else goFurther = false;

	} while ((goFurther));
	return possible_destinations;
}