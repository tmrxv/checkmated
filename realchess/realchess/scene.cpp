#include "scene.h"

Scene::Scene()
{
}

Scene::Scene(GUI* gui)
{
	this->gui = gui;
	state = STARTING;
}

Scene::~Scene()
{
}

SceneState Scene::get_state()
{
	return state;
}


void Scene::set_state(SceneState new_state)
{
	state = new_state;
}