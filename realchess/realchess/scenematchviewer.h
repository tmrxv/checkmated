#pragma once
#include "scene.h"
#include "board.h"
#include <fstream>
#include <sstream>
#include "gui.h"

class SceneMatchViewer : public Scene
{
private:
	vector<string> notation;
	Board* board;
	int moveNr = 0;
	bool whiteTurn = true;
	array<char, 8> datax;
	array<char, 8> datay;
	array<char, 5> first_letters;
	array<FigureType, 5> figure_types;

public:
	SceneMatchViewer();
	SceneMatchViewer(GUI*, Board*);
	virtual ~SceneMatchViewer();

	//functions
	void draw(RenderWindow& window);
	void removeExtraLetters();
	void readPGNFile();
	void event_handling(int, Vector2f);
	Vector2i searchFigure(FigureType, Vector2i);
	Vector2i searchFigure(FigureType, Vector2i, char);
	Vector2i pawnException(char, int);
	int translateCoordinate(char);
	bool isRow(char);
	void startup();
	void shutdown();
};

