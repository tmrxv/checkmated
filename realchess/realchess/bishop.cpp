#include "bishop.h"

Bishop::Bishop(bool is_white)
{
	type = BISHOP;
	this->is_white = is_white;
}

Bishop::~Bishop()
{

}

vector<Vector2i> Bishop::get_possible_destinations(Vector2i my, boardContent& content)
{
	vector<Vector2i> possible_destinations;
	vector<Vector2i> help;

	for (int x = -1; x <= 2; x += 2)
	{
		for (int y = -1; y <= 2; y += 2)
		{
			help = get_linear_destinations(my, Vector2i(x, y), content);
			if (!help.empty())
			{
				for (auto d : help)
				{
					possible_destinations.push_back(d);
				}
				help.clear();
			}
		}
	}
	return possible_destinations;
}