#include "menu.h"
#include <IOSTREAM>

Menu::Menu()
{
}

Menu::Menu(string name, Font& font, Color color)
{
	this->name = name;
	this->color = color;
	this->font = font;

	gui::Sprite* spr_selection = new gui::Sprite();
	Texture* texture_selection = new Texture();
	texture_selection->loadFromFile("images/gui/main_menu/selection_entries.png");;
	spr_selection->setTexture(*texture_selection, false);
	spr_selection->setColor(color);

	gui::Sprite* spr_triangle = new gui::Sprite();
	Texture* texture_triangle = new Texture();
	texture_triangle->loadFromFile("images/gui/main_menu/triangle.png");;
	spr_triangle->setTexture(*texture_triangle, false);
	spr_triangle->setPosition(Vector2f(512, 24));

	selection.push_back(spr_selection);
	selection.push_back(spr_triangle);
	selection.setAlpha(0);
}

Menu::~Menu()
{
}

MenuEntry* Menu::getSelectedEntry()
{
	return selectedEntry;
}

void Menu::push_back_entry(MenuEntry* entry)
{
	entry->setPosition(positionFirstEntry + Vector2f(offsetEntry.x * numberOfEntries, offsetEntry.y * numberOfEntries));
	entry->setColor(color);
	entries.push_back(entry);
	buttons.push_back(FloatRect(entry->getPosition() - Vector2f(15, 15), Vector2f(615, 120)));
	numberOfEntries++;
}

Color Menu::getColor()
{
	return color;
}

string Menu::getName()
{
	return name;
}

void Menu::fadeInEntries(int speed)
{
	for (auto entry : entries)
	{
		entry->fade_in(speed);
	}
}

void Menu::fadeOutEntries(int speed)
{
	for (auto entry : entries)
	{
		entry->fade_out(speed);
	}
	selection.fade_out(speed);
}

void Menu::drawMe(RenderWindow& window)
{
	//draw panel
	objects.at(0)->draw(window, position);
	//draw entries (label and text shadow)
	for (auto entry : entries)
	{
		entry->draw(window, position, 0, 2);
	}
	//draw selection inbetween
	selection.draw(window, position);
	//draw entries (text and icon)
	for (auto entry : entries)
	{
		entry->draw(window, position, 3, 4);
	}
}

void Menu::eventHandling(int mouse_leftClick, Vector2f mouse_coordinates)
{
	if (inputAllowed)
	{
		if (mouse_leftClick == 1)
		{
			for (int i = 0; i < buttons.size(); i++)
			{
				if (buttons.at(i).contains(mouse_coordinates))
				{
					//nothing selected previously
					if (selectedEntry == NULL)
					{
						selection.setPosition(entries.at(i)->getPosition());
						selection.fade_in(20);
					}
					else
					{
						//double click
						if (selectedEntry == entries.at(i))
						{
							changeScene = true;
							selection.setPosition(entries.at(i)->getPosition());
							selection.setAlpha(255);
						}
						//select another entry
						else
						{
							selection.move_to(entries.at(i)->getPosition(), 25, SLOWDOWN);
						}
					}
					selectedEntry = entries.at(i);
				}
			}
		}
	}

	for (auto entry : entries)
	{
		if (entry == selectedEntry)
		{
			if (selection.getPosition().y > entry->getPosition().y - 70 &&
				selection.getPosition().y < entry->getPosition().y + 70)
			{
				entry->select();
			}
		}
		else
		{
			entry->unselect();
		}
		entry->update();
	}
	selection.update();
}


bool Menu::sceneChanging()
{
	return changeScene;
}

void Menu::unselect()
{
	selection.setAlpha(0);
	selectedEntry = NULL;
}