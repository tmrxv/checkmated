#pragma once
#include "Lection.h"
class LectionCastle :
	public Lection
{
private:
	vector<vector<Vector2i>> positions;
public:
	LectionCastle();
	LectionCastle(string);
	virtual ~LectionCastle();
	virtual void event_handling(Board*, int);
};