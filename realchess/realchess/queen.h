#pragma once
#include "figureLinear.h"

class Queen: public FigureLinear
{
public:
	//con-/destructor
	Queen(bool);
	virtual ~Queen();

	//methods
	vector<Vector2i> get_possible_destinations(Vector2i, boardContent&);
};

