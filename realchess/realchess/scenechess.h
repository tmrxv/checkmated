#pragma once
#include "scene.h"
#include "container.h"
#include "label.h"
#include "playerpanel.h"
#include "board.h"
#include "gui.h"

class GUI;
class PlayerPanel;
class Trainer;
class SceneChess : public Scene
{
private:
	Font font;
	Board* board;
	PlayerPanel* player1Panel;
	PlayerPanel* player2Panel;

public:
	SceneChess();
	SceneChess(GUI*, Board*, Trainer*);
	virtual ~SceneChess();

	//functions
	void draw(RenderWindow& window);
	void event_handling(int, Vector2f);
	void startup();
	void shutdown();
};