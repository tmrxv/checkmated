#include "LectionPawn.h"
#include <stdlib.h>

LectionPawn::LectionPawn(string name)
{
	this->name = name;
	font.loadFromFile("C:/Windows/Fonts/calibri.ttf");

	positions =
	{ {
		Vector2i(3, 6), { 3, 6 }, Vector2i(3, 6), { 3, 4 }, Vector2i(5, 1), { 5, 2 },
		Vector2i(3, 4), { 3, 4 }, Vector2i(3, 4), { 3, 3 }, Vector2i(5, 2), { 5, 3 },
		Vector2i(3, 3), { 3, 2 }, Vector2i(4, 1), { 4, 1 }, Vector2i(4, 1), { 3, 2 }
	} };

	textOrder = { 1, 3, 7, 9 };
	stdContent = true;
}

LectionPawn::LectionPawn()
{

}

LectionPawn::~LectionPawn()
{

}

void LectionPawn::event_handling(Board* board, int mouse_click)
{
	show_text(mouse_click);

	if (mouse_click == 1)
	{
		if (move <= positions.size())
		{
			move_figure(board, mouse_click, positions[move - 1][0], positions[move - 1][1]);
		}
		else
		{
			boardInteractable = true;
		}
	}
}


