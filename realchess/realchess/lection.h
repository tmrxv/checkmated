#pragma once
#include <string>
#include "board.h"
#include "label.h"
#include "sprite.h"
#include "text.h"
#include <iostream>
using namespace std;
using namespace sf;

class Lection
{
protected:
	string name;
	boardContent content;
	Font font;
	FILE* file;
	char* ptr;
	char input[200];
	int nLines;
	vector<int> textOrder;
	int move = 0;
	bool showText = false;
	bool boardInteractable = false;
	bool stdContent;
	vector<gui::Text> lectionInfos;
	//functions
	void move_figure(Board*, int, Vector2i, Vector2i);
	void show_text(int);
public:
	Lection();
	Lection(string);
	virtual ~Lection();
	boardContent& get_content();
	bool check_if_content_is_std();
	string get_name();
	vector<gui::Text> get_lection_infos();
	virtual void event_handling(Board*, int) = 0;
	bool showInformation();
	bool boardIsInteractable();
};

