#include "menuentry.h"
#include <iostream>

MenuEntry::MenuEntry()
{
}

MenuEntry::~MenuEntry()
{
	delete panelIcon.getTexture();
	delete panelText.getTexture();
	delete icon.getTexture();
}

MenuEntry::MenuEntry(string text, Font& font, string iconName)
{
	//panel icon
	Texture* texturePanelIcon = new Texture();
	texturePanelIcon->loadFromFile("images/gui/main_menu/entry_icon.png");
	panelIcon.setTexture(*texturePanelIcon, true);
	//panel text
	Texture* texturePanelText = new Texture();
	texturePanelText->loadFromFile("images/gui/main_menu/entry_text.png");
	panelText.setTexture(*texturePanelText, true);
	//icon
	Texture* textureIcon = new Texture();
	textureIcon->loadFromFile("images/gui/main_menu/" + iconName + ".png");
	icon.setTexture(*textureIcon, true);
	icon.setPosition(Vector2f(13, 0));
	//text
	this->text.setFont(font);
	this->text.setFillColor(Color::Black);
	this->text.setCharacterSize(63);
	this->text.setStyle(Text::Italic);
	this->text.setString(text);
	this->text.setPosition(offsetText);
	//text shadow
	textShadow.setFont(font);
	textShadow.setFillColor(Color(200, 200, 200, 255));
	textShadow.setCharacterSize(63);
	textShadow.setStyle(Text::Italic);
	textShadow.setString(text);
	textShadow.setPosition(offsetText + Vector2f(2, 2));

	parts.push_back(&panelText);
	parts.push_back(&panelIcon);
	parts.push_back(&textShadow);
	parts.push_back(&this->text);
	parts.push_back(&icon);
}

void MenuEntry::select()
{
	if (!selected)
	{
		text.setFillColor(Color::White);
	}
	selected = true;
}

void MenuEntry::unselect()
{
	if (selected)
	{
		text.setFillColor(Color::Black);
	}
	selected = false;
}

string MenuEntry::getInformation()
{
	return information;
}

void MenuEntry::setScene(SceneName sceneName)
{
	myScene = sceneName;
}

SceneName MenuEntry::getScene()
{
	return myScene;
}

void MenuEntry::setInformation(string information)
{
	this->information = information;
}

void MenuEntry::setColor(Color color)
{
	panelIcon.setColor(color);
}

void MenuEntry::event_handling(int, Vector2i)
{

}

void MenuEntry::setLocked()
{
	locked = true;
}

//tag der offenen t�r
bool MenuEntry::getLocked()
{
	return locked;
}