#pragma once
#include "graphicalobject.h"

namespace gui
{
	class Text : public GraphicalObject
	{
	private:
		sf::Text text;

		//functions
		void setAlphas();

	public:
		Text();
		Text(Font&, Color, int);
		Text(Font&, Color, int, Vector2f);
		Text(const gui::Text&);
		virtual ~Text();

		//adapter functions
		void setFillColor(Color);
		void setString(std::string);
		string getString() const;
		void setFont(Font&);
		void setCharacterSize(int);
		void setLetterSpacing(float);
		void setLineSpacing(float);
		void setStyle(Uint32);
		void setRotation(float);
		void setOrigin(Vector2f);
		FloatRect getLocalBounds() const;
		FloatRect getGlobalBounds() const;
		sf::Text getTextObject() const;

		//functions
		void draw(RenderWindow&, Vector2f);
		void draw(RenderWindow&);
		void update();
	};
}