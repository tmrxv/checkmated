#pragma once
#include "figureLinear.h"

class Castle: public FigureLinear
{
private:
	bool moved_already;
	vector<Vector2i> castle_start_positions;
public:
	//con-/destructor
	Castle(bool);
	virtual ~Castle();

	//methods
	vector<Vector2i> get_possible_destinations(Vector2i, boardContent&);
	bool get_moved_already();
	void figure_already_moved();
};

