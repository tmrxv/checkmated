#pragma once
#include <SFML/Graphics.hpp>

using namespace sf;

class Fadable
{
protected:
	int alpha = 255;
	int destination_alpha = 255;
	int speed_alpha = 5;
	bool fading = true;
	bool effectedByGlobalAlpha = true;

	//functions
	virtual void setAlphas() = 0;
public:
	Fadable();
	virtual ~Fadable();

	//functions
	void setEffectedByGlobalAlpha(bool);
	bool isEffectedByGlobalAlpha();
	void do_fading();
	void fade_in(int);
	void fade_out(int);
	void fade_to(int, int);
	void setAlpha(int);
};