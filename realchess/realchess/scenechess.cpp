#include "scenechess.h"

SceneChess::SceneChess()
{
}

SceneChess::SceneChess(GUI* gui, Board* board, Trainer* trainer) : Scene(gui)
{
	this->board = board;
	font.loadFromFile("C:/Windows/Fonts/calibrib.ttf");

	player1Panel = new PlayerPanel(font, false, gui, board);
	player2Panel = new PlayerPanel(font, false, gui, board);

	player1Panel->setPosition(Vector2f(720, 720));

	//Label* black_3 = new Label(col_grey, Color::White, Vector2f(29, 177), Vector2f(234, 60), "1", font, 0, false);
	startup();
}

SceneChess::~SceneChess()
{
	//todo delete

}

void SceneChess::draw(RenderWindow& window)
{
	player1Panel->drawMe(window);
}

void SceneChess::event_handling(int left_mouse_click, Vector2f m_coordinates_fields)
{
	if (state == STARTING)
	{
		if (board->is_triggered())
		{
			gui->setBoardInteractable(true);
			state = RUNNING;
		}
	}
	else if (state == RUNNING)
	{
		if (board->new_notation_available())
		{
			player1Panel->addNotation(board->get_notation());
		}
		player1Panel->event_handling(left_mouse_click, m_coordinates_fields);
	}
	player1Panel->update();
}

void SceneChess::startup()
{
	gui->showBoard();
	player1Panel->move_by(Vector2f(0, -696), 12, SLOWDOWN);
}

void SceneChess::shutdown()
{
}