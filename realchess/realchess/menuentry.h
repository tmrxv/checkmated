#pragma once
#include "component.h"
#include "text.h"
#include "sprite.h"

enum  SceneName;
class MenuEntry : public Component
{
private:
	gui::Sprite panelIcon;
	gui::Sprite panelText;
	gui::Sprite icon;
	gui::Text text;
	gui::Text textShadow;
	string information;
	const Vector2f offsetText = Vector2f(135, 3);
	bool selected = false;
	SceneName myScene;

	//tdot
	bool locked = false;

public:
	MenuEntry();
	MenuEntry(string, Font&, string);
	virtual ~MenuEntry();

	//functions
	void select();
	void unselect();
	void setScene(SceneName);
	SceneName getScene();
	void setInformation(string);
	string getInformation();
	void setColor(Color);
	void event_handling(int, Vector2i);
	//tag der offenen t�r
	void setLocked();
	bool getLocked();
};