#pragma once
#include "fadable.h"
#include "movable.h"
#include "graphicalobject.h"

class Component : public Fadable, public Movable
{
protected:
	vector<GraphicalObject*> parts;

	//fumnctions
	void setAlphas();

public:
	Component();
	virtual ~Component();

	//functions
	void push_back(GraphicalObject*);
	void draw(RenderWindow&);
	void draw(RenderWindow&, int, int);
	void draw(RenderWindow&, Vector2f);
	void draw(RenderWindow&, Vector2f, int, int);
	void update();
};