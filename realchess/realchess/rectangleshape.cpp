#include "rectangleshape.h"

gui::RectangleShape::RectangleShape()
{
}

gui::RectangleShape::~RectangleShape()
{
}

gui::RectangleShape::RectangleShape(Vector2f size)
{
	rect_shape = sf::RectangleShape(size);
}

void gui::RectangleShape::setAlphas()
{
	sf::Color col = rect_shape.getFillColor();
	col.a = alpha;
	rect_shape.setFillColor(col);
}

void gui::RectangleShape::update()
{
	do_fading();
	do_moving();
}

void gui::RectangleShape::setFillColor(Color color)
{
	alpha = color.a;
	destination_alpha = color.a;
	rect_shape.setFillColor(color);
}

FloatRect gui::RectangleShape::getLocalBounds()
{
	return rect_shape.getGlobalBounds();
}

Color gui::RectangleShape::getFillColor()
{
	return rect_shape.getFillColor();
}

void gui::RectangleShape::setSize(Vector2f size)
{
	rect_shape.setSize(size);
}

Vector2f gui::RectangleShape::getSize()
{
	return rect_shape.getSize();
}

void gui::RectangleShape::draw(RenderWindow& window)
{
	draw(window, Vector2f(0, 0));
}

void gui::RectangleShape::draw(RenderWindow& window, Vector2f offset)
{
	rect_shape.setPosition(position + offset);
	window.draw(rect_shape);
	rect_shape.setPosition(position);
}

void gui::RectangleShape::setOutlineColor(Color color)
{
	rect_shape.setOutlineColor(color);
}

void gui::RectangleShape::setOutlineThickness(float thickness)
{
	rect_shape.setOutlineThickness(thickness);
}

void gui::RectangleShape::setOrigin(Vector2f origin)
{
	rect_shape.setOrigin(origin);
}

void gui::RectangleShape::setRotation(float angle)
{
	rect_shape.setRotation(angle);
}