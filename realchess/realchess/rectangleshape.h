#pragma once
#include "graphicalobject.h"

namespace gui
{
	class RectangleShape : public GraphicalObject
	{
	private:
		sf::RectangleShape rect_shape;

		//functions
		void setAlphas();

	public:
		RectangleShape();
		RectangleShape(Vector2f);
		virtual ~RectangleShape();

		//adapter functions
		void setFillColor(Color);
		Color getFillColor();
		void setSize(Vector2f);
		FloatRect getLocalBounds();
		Vector2f getSize();
		void setOutlineColor(Color);
		void setOutlineThickness(float);
		void setOrigin(Vector2f);
		void setRotation(float);

		//functions
		void draw(RenderWindow&, Vector2f);
		void draw(RenderWindow&);
		void update();
	};
}