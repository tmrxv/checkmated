#pragma once
#include "figureLinear.h"
#include "castle.h"

class King : public FigureLinear
{
private:
	bool moved_already;
	bool checked;

	//functions
	void get_possible_destinations_for_rochade(Vector2i, boardContent, vector<Vector2i>&);

public:
	//con-/destructor
	King(bool);
	virtual ~King();

	//methods
	vector<Vector2i> get_possible_destinations(Vector2i, boardContent&);
	void figure_already_moved();
	bool get_moved_already();
	void set_checked(bool);
};