#pragma once
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "figure.h"
#include <SFML/Graphics.hpp>

using namespace std;
using namespace sf;

class Notator
{
private:
	array<char, 8> datax;
	array<char, 8> datay;
	array<char, 5> first_letters;
	vector<Vector2i> destinations_of_figure;
	FILE* file;
	string c_note_string;
	int turn = 0;
	int ctr = 0;
	char initial_field;
	bool special_case = false;
	bool white_turn = false;
	bool enemy_on_field = false;
	bool left_rochade = false;
	bool right_rochade = false;
	bool end_of_game = false;
	bool promoted = false;
	string note;
	const char* c_note = nullptr;
	bool check_special_case(int, int, Figure*, Vector2i, Vector2i, vector<Vector2i>, boardContent&);
public:
	Notator();
	virtual ~Notator();

	void set_destinations(vector<Vector2i>);
	void write_data(Vector2i, Figure*, Vector2i, boardContent&, vector<string>&);
	void put_char(int, int, int, vector<string>&);
	void set_string(string);
	void set_booleans(bool, bool, bool, bool, bool, bool);
};