#include "LectionCastle.h"

LectionCastle::LectionCastle(string name)
{
	this->name = name;
	font.loadFromFile("C:/Windows/Fonts/calibri.ttf");

	positions =
	{ {
		Vector2i(7, 6), { 7, 4 }, Vector2i(0, 1), { 0, 3 },
		Vector2i(7, 7), { 7, 5 }, Vector2i(0, 0), { 0, 2 },
		Vector2i(7, 5), { 2, 5 }, Vector2i(0, 2), { 5, 2 },
		Vector2i(2, 5), { 2, 3 }, Vector2i(5, 2), { 5, 3 },
		Vector2i(2, 3), { 2, 3 }, Vector2i(2, 3), { 1, 3 }, Vector2i(5, 3), { 5, 3 }, Vector2i(5, 3), { 1, 3 }
	} };

	stdContent = true;
}

LectionCastle::LectionCastle()
{

}

LectionCastle::~LectionCastle()
{

}

void LectionCastle::event_handling(Board* board, int mouse_click)
{
	show_text(mouse_click);

	if (mouse_click == 1)
	{
		if (move <= positions.size())
		{
			if (move == 1 || move == 8 || move == 11 || move == 12)
			{
				showText = true;
			}

			move_figure(board, mouse_click, positions[move - 1][0], positions[move - 1][1]);
		}
		else
		{
			boardInteractable = true;
		}
	}
}
