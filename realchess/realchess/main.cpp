#pragma once
#include <iostream>
#include "gui.h"

using namespace sf;
using namespace std;

//functions
void loop(RenderWindow&);
void get_input(RenderWindow&);

//inits
ContextSettings settings;
Vector2f m_coordinates_pixels;
GUI gui_system;
int mouse_leftClick = 0;

int main()
{
	//create window and set graphical settings
	settings.antialiasingLevel = 8;
	RenderWindow window(VideoMode(1280, 720),
		"Checkmated - Schachsoftware", Style::Default, settings);
	window.setFramerateLimit(60);
	window.setVerticalSyncEnabled(true);

	//titlebar icon
	Image icon = Image();
	icon.loadFromFile("images/icon_titlebar.png");
	window.setIcon(20, 20, icon.getPixelsPtr());

	//loop
	while (window.isOpen())
	{
		Event event;
		while (window.pollEvent(event))
		{
			//if window is closed
			if (event.type == Event::Closed)
				window.close();
		}
		loop(window);
	}
	return 0;
}

void loop(RenderWindow& window)
{
	get_input(window);
	gui_system.event_handling(mouse_leftClick, m_coordinates_pixels);
	gui_system.draw(window);
}

void get_input(RenderWindow& window)
{
	//get click
	if (Mouse::isButtonPressed(Mouse::Left))
	{
		mouse_leftClick++;
	}
	else
	{
		mouse_leftClick = 0;
	}

	//get coordinates
	m_coordinates_pixels = (Vector2f) Mouse::getPosition(window);

	//correct coordinates if out of bounds
	if (m_coordinates_pixels.x < 0)
	{
		m_coordinates_pixels.x = 0;
	}
	else if (m_coordinates_pixels.x > window.getSize().x)
	{
		m_coordinates_pixels.x = (float)window.getSize().x;
	}
	if (m_coordinates_pixels.y < 0)
	{
		m_coordinates_pixels.y = 0;
	}
	else if (m_coordinates_pixels.y > window.getSize().y)
	{
		m_coordinates_pixels.y = (float)window.getSize().y;
	}
}