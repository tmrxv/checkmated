#pragma once
#include <SFML/Graphics.hpp>
#include "lection.h"

class GUI;
enum SceneState
{
	STARTING, RUNNING, CLOSING
};

class Scene
{
protected:
	//variables
	GUI* gui;
	SceneState state;

public:
	//con-/destructor
	Scene();
	Scene(GUI*);
	virtual ~Scene();

	//functions
	virtual void draw(sf::RenderWindow&) = 0;
	virtual void event_handling(int, sf::Vector2f) = 0;
	virtual void startup() = 0;
	virtual void shutdown() = 0;
	SceneState get_state();
	void set_state(SceneState);
};