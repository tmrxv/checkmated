#pragma once
#include "scene.h"
#include "board.h"
#include "gui.h"

class SceneManager : public Scene
{
	Board* board;
	Font font;
	gui::Text menu_name;
	Container cont_menu;
	gui::RectangleShape panel_white;
	gui::Sprite toolbar;
	gui::Sprite arrow_left;
	gui::Sprite arrow_right;


public:
	SceneManager();
	SceneManager(GUI*, Board*);
	virtual ~SceneManager();

	//functions
	void draw(RenderWindow& window);
	void event_handling(int, Vector2f);
	void startup();
	void shutdown();
};