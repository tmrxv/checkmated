#pragma once
#include "Lection.h"

class LectionPawn : public Lection
{
private:
	vector<vector<Vector2i>> positions;
public:
	LectionPawn();
	LectionPawn(string);
	virtual ~LectionPawn();
	virtual void event_handling(Board*, int);
};