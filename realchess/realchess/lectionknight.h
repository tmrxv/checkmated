#pragma once
#include "Lection.h"
class LectionKnight :
	public Lection
{
private:
	vector<vector<Vector2i>> positions;
public:
	LectionKnight();
	LectionKnight(string);
	virtual ~LectionKnight();
	virtual void event_handling(Board*, int);
};