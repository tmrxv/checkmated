#include "gui.h"

GUI::GUI()
{
	background = gui::Sprite("images/gui/main_menu/background.png");
	background_colored_lower.setSize(Vector2f(1280, 720));
	background_colored_upper.setSize(Vector2f(1280, 720));

	board.setPosition(Vector2f(-720, 0));

	currentScene = new SceneMainMenu(this, 0, NULL);
}

GUI::~GUI()
{
}

void GUI::changeColor(Color color)
{
	background_colored_lower.setFillColor(background_colored_upper.getFillColor());
	background_colored_lower.fade_out(10);
	background_colored_upper.setFillColor(color);
	background_colored_upper.setAlpha(0);
	background_colored_upper.fade_to(150, 10);
}

void GUI::setBoardInteractable(bool interactable)
{
	boardInteractable = interactable;
}

void GUI::changeScene(SceneName scene)
{
	delete currentScene;
	if (scene == CHESS)
	{
		currentScene = new SceneChess(this, &board, &trainer);
	}
	else if (scene == SELECTION)
	{
		currentScene = new SceneSelection(this, &board, &trainer);
	}
	else if (scene == TRAINING)
	{
		currentScene = new SceneTraining(this, &board, &trainer);
	}
	this->scene = scene;
}

void GUI::hideBoard()
{
	board.move_to(Vector2f(-2000, 0), 4, ACCELERATE);
	boardInteractable = false;
}

void GUI::showBoard()
{
	board.setPosition(Vector2f(-720, 0));
	board.move_to(Vector2f(0, 0), 12, SLOWDOWN);
}

void GUI::event_handling(int left_mouse_click, Vector2f mouse_coordinates)
{
	if (boardInteractable)
	{
		board.event_handling(left_mouse_click, mouse_coordinates);
	}
	currentScene->event_handling(left_mouse_click, mouse_coordinates);
	board.updateMe();
	background_colored_lower.update();
	background_colored_upper.update();
}

void GUI::draw(RenderWindow& window)
{
	window.clear();
	background.draw(window);
	background_colored_lower.draw(window);
	background_colored_upper.draw(window);
	if (scene == CHESS)
	{
		board.drawMe(window);
	}
	currentScene->draw(window);
	window.display();
}