#include "text.h"

gui::Text::Text()
{
}

gui::Text::Text(Font& font, Color color, int size)
{
	setFont(font);
	setFillColor(color);
	setCharacterSize(size);
}

gui::Text::Text(Font& font, Color color, int size, Vector2f position)
{
	setFont(font);
	setFillColor(color);
	setCharacterSize(size);
	setPosition(position);
}

gui::Text::~Text()
{
}

gui::Text::Text(const gui::Text& t)
{
	text = t.getTextObject();
}

void gui::Text::setAlphas()
{
	sf::Color col = text.getFillColor();
	col.a = alpha;
	text.setFillColor(col);
}

void gui::Text::update()
{
	do_fading();
	do_moving();
}

void gui::Text::setRotation(float angle)
{
	text.setRotation(angle);
}

void gui::Text::setOrigin(Vector2f origin)
{
	text.setOrigin(origin);
}

void gui::Text::draw(RenderWindow& window)
{
	draw(window, Vector2f(0, 0));
}

void gui::Text::draw(RenderWindow& window, Vector2f offset)
{
	text.setPosition(position + offset);
	window.draw(text);
}

void gui::Text::setFillColor(Color color)
{
	alpha = color.a;
	text.setFillColor(color);
}

void gui::Text::setString(std::string text)
{
	this->text.setString(text);
}

void gui::Text::setFont(Font& font)
{
	text.setFont(font);
}

string gui::Text::getString() const
{
	return text.getString();
}

void gui::Text::setCharacterSize(int size)
{
	text.setCharacterSize(size);
}

FloatRect gui::Text::getLocalBounds() const
{
	return text.getLocalBounds();
}

FloatRect gui::Text::getGlobalBounds() const
{
	return text.getGlobalBounds();
}

void gui::Text::setLetterSpacing(float spacingFactor)
{
	text.setLetterSpacing(spacingFactor);
}

void gui::Text::setLineSpacing(float spacingFactor)
{
	text.setLineSpacing(spacingFactor);
}

sf::Text gui::Text::getTextObject() const
{
	return text;
}

void gui::Text::setStyle(Uint32 style)
{
	text.setStyle(style);
}