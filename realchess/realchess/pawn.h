#pragma once
#include "figure.h"

class Pawn : public Figure
{
private:
	//variables
	int direction;
	bool en_passant;

public:
	//con-/destructor
	Pawn(bool);
	virtual ~Pawn();

	//methods
	vector<Vector2i> get_possible_destinations(Vector2i, boardContent&);
	int get_direction();
	bool get_en_passant();
	void set_en_passant(bool);
};