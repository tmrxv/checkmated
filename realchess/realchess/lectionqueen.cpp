#include "LectionQueen.h"

LectionQueen::LectionQueen(string name)
{
	this->name = name;
	font.loadFromFile("C:/Windows/Fonts/calibri.ttf");

	positions =
	{ {
		Vector2i(2, 6), { 2, 5 }, Vector2i(4, 1), { 4, 2 },
		Vector2i(3, 7), { 1, 5 }, Vector2i(3, 0), { 7, 4 },
		Vector2i(1, 5), { 1, 4 }, Vector2i(7, 4), { 6, 4 },
		Vector2i(1, 4), { 1, 4 }, Vector2i(1, 4), { 6, 4 }
	} };

	stdContent = true;
}

LectionQueen::LectionQueen()
{

}

LectionQueen::~LectionQueen()
{

}

void LectionQueen::event_handling(Board* board, int mouse_click)
{
	show_text(mouse_click);

	if (mouse_click == 1)
	{
		if (move <= positions.size())
		{
			move_figure(board, mouse_click, positions[move - 1][0], positions[move - 1][1]);
		}
		else
		{
			boardInteractable = true;
		}
	}
}