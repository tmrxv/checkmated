#include "scenemanager.h"

SceneManager::SceneManager()
{
}

SceneManager::~SceneManager()
{
}

SceneManager::SceneManager(GUI* gui, Board* board) : Scene(gui)
{
	this->board = board;

	font.loadFromFile("C:/Windows/Fonts/calibrib.ttf");

	arrow_left = gui::Sprite("images/gui/matchviewer/arrow_left.png", Vector2f(-102, 306));
	arrow_right = gui::Sprite("images/gui/matchviewer/arrow_right.png", Vector2f(1280, 306));
	toolbar = gui::Sprite("images/gui/matchviewer/toolbar.png", Vector2f(1280, 55));

	panel_white = gui::RectangleShape(Vector2f(1280, 567));
	panel_white.setAlpha(185);

	menu_name = gui::Text(font, Color::White, 84);
	menu_name.setPosition(Vector2f(40, 20));
	menu_name.setString("PGN-Manager");
	menu_name.setStyle(Text::Underlined);

	cont_menu.push_back(&panel_white);
	cont_menu.setPosition(Vector2f(0, 720));

	startup();
}


void SceneManager::draw(RenderWindow& window)
{
	panel_white.draw(window);
	arrow_left.draw(window);
	arrow_right.draw(window);
	menu_name.draw(window);
	toolbar.draw(window);
}

void SceneManager::event_handling(int left_mouse_click, Vector2f m_coordinates_fields)
{
	if (state == STARTING)
	{
		if (panel_white.is_triggered())
		{
			toolbar.move_by(Vector2f(-toolbar.getLocalBounds().width, 0), 12, SLOWDOWN);
			state = RUNNING;
		}
	}
	else if (state == RUNNING)
	{

	}
	else if (state == CLOSING)
	{

	}
	arrow_left.update();
	arrow_right.update();
	panel_white.update();
	menu_name.update();
	toolbar.update();
}

void SceneManager::startup()
{
	panel_white.move_by(Vector2f(0, -567), 12, SLOWDOWN);
}

void SceneManager::shutdown()
{

}