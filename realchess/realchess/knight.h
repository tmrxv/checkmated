#pragma once
#include "figure.h"

class Knight : public Figure
{
public:
	//con-/destructor
	Knight(bool);
	virtual ~Knight();

	//methods
	vector<Vector2i> get_possible_destinations(Vector2i, boardContent&);
};