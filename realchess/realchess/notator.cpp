#include "notator.h"
#include <sstream>

Notator::Notator()
{
	datax = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h' };
	datay = { '8', '7', '6', '5', '4', '3', '2', '1' };
	first_letters = { 'N', 'R', 'B', 'K', 'Q' };
}

Notator::~Notator()
{

}

void Notator::write_data(Vector2i clicked_field, Figure* selected_figure, Vector2i selected_coordinates, boardContent& content, vector<string>& notation)
{
	if (selected_figure == nullptr) {
		//TODO maybe error handling
		return;
	}
	//get type
	int type = selected_figure->get_type();
	bool white = selected_figure->check_if_white();
	//other figure
	bool twin_white = false;
	int twin_type = 0;
	content[clicked_field.y][clicked_field.x] = NULL;

	for (int x = 0; x < 8; x++)
	{
		for (int y = 0; y < 8; y++)
		{
			if (type == PAWN)
			{	//initial field is needed if pawn eats
				if (selected_coordinates.x == x && selected_coordinates.y == y)
					initial_field = datax[x];
			}
			else if (type == KNIGHT || type == CASTLE)
			{
				if (content[y][x] != NULL)
				{
					Vector2i twin_coordinates = { x, y };
					//twin must not be selected figure
					if (selected_coordinates != twin_coordinates)
					{
						//other figure must be a twin
						twin_white = content[y][x]->check_if_white();
						twin_type = content[y][x]->get_type();
						if (twin_type == type && twin_white && white && !white_turn)
							//special case
							special_case = check_special_case(x, y, selected_figure, clicked_field, selected_coordinates, destinations_of_figure, content);
					}
				}
			}
		}
	}
	if (content[clicked_field.y][clicked_field.x] == NULL)
		content[clicked_field.y][clicked_field.x] = selected_figure;

	for (int x = 0; x < 8; x++)
	{
		for (int y = 0; y < 8; y++)
		{
			if (clicked_field.x == x && clicked_field.y == y)
				put_char(type, x, y, notation);	//set applicable chars
		}
	}

	if (end_of_game)
	{
		file = fopen("data.txt", "a");
		if (file == NULL)
		{
			fprintf(stderr, "Error, could not open file data.txt\n");
			exit(1); //error
		}
		else
		{
			fprintf(stderr, "Successfully entered data.txt from notator\n");
			// show notation at the end
			for (int i = 0; i < notation.size(); i++)
			{
				if (ctr % 2 == 0 && notation.at(i) != "0-1")
				{
					turn++;
					c_note_string = to_string(turn);
					c_note = c_note_string.c_str();
					if (ctr != 0)
						fputs("\n", file);
					fputs(c_note, file);
					fputs(". ", file);
				}
				ctr++;
				c_note = (notation.at(i)).c_str();
				fputs(c_note, file);
				putc(' ', file);
			}
		}
		fclose(file);
	}
}

bool Notator::check_special_case(int x, int y, Figure* selected_figure, Vector2i clicked_field, Vector2i selected_coordinates, vector<Vector2i> destinations, boardContent& content)
{
	//twin
	Vector2i twin_coordinates = { x, y };
	vector<Vector2i> destinations_of_twin = content[y][x]->get_possible_destinations(twin_coordinates, content);
	content[clicked_field.y][clicked_field.x] = selected_figure;

	for (int i = 0; i < destinations.size(); i++)
	{
		for (int j = 0; j < destinations_of_twin.size(); j++)
		{
			//check special case
			if (destinations.at(i) == destinations_of_twin.at(j))
			{
				Vector2i field_dest = { destinations.at(i).x, destinations.at(i).y };
				if (field_dest == clicked_field)
				{
					if (twin_coordinates.y == selected_coordinates.y) //same row
					{
						initial_field = datax[selected_coordinates.x];
						return true;
					}
					else if (twin_coordinates.x == selected_coordinates.x) // same line
					{
						initial_field = datay[selected_coordinates.y];
						return true;
					}
					else //prefer 1. variant
					{
						initial_field = datax[selected_coordinates.x];
						return true;
					}
				}
			}
		}
	}
	return false;
}

void Notator::set_booleans(bool white, bool enemy, bool pawn_promotion, bool left, bool right, bool end)
{
	white_turn = white;
	enemy_on_field = enemy;
	promoted = pawn_promotion;
	left_rochade = left;
	right_rochade = right;
	end_of_game = end;
}

void Notator::set_string(string notation)
{
	note = notation;
}

void Notator::put_char(int type, int x, int y, vector<string>& notation)
{
	stringstream ss;

	if (!left_rochade && !right_rochade && !end_of_game)
	{
		//set first letter
		if (type != PAWN && !promoted)
			ss << first_letters.at(type - 1); // -1 because nothing for pawn

		if ((type == PAWN && enemy_on_field) || promoted)
		{
			ss << initial_field;
		}
		//these 2 types have special cases
		else if (type == KNIGHT || type == CASTLE)
		{
			if (special_case)
				ss << initial_field;
			special_case = false;
		}
		// 'x' if enemy was eaten
		if (enemy_on_field)
		{
			ss << 'x';
		}
		//coordinates of arrived field
		ss << datax[x] << datay[y];
		if (note.empty())
		{
			notation.push_back(ss.str());
		}
	}
	if (!note.empty())
	{
		ss << note;
		notation.push_back(ss.str());
		note.clear();
	}
}

void Notator::set_destinations(vector<Vector2i> destinations)
{
	destinations_of_figure.clear();
	destinations_of_figure = destinations;
}