#pragma once
#include "figure.h"

class FigureLinear : public Figure
{
protected:
	//variables
	int max_steps = 7;

	//methods
	vector<Vector2i> get_linear_destinations(Vector2i, Vector2i, boardContent&);

public:
	//de-/constructor
	FigureLinear();
	virtual ~FigureLinear();
};