#include "board.h"
#include <iostream>

using namespace sf;
using namespace std;

Board::Board()
{
	//text
	font.loadFromFile("C:/Windows/Fonts/calibri.ttf");
	notator_strings = { "O-O", "O-O-O", "=Q", "#", "+", "1/2-1/2", "1-0", "0-1" };
	castling_destinations = { Vector2i(2, 0), Vector2i(6, 0), Vector2i(2, 7), Vector2i(6, 7) };

	gui::Text* text1 = new gui::Text();
	text1->setFillColor(Color::White);
	text1->setFont(font);
	text1->setStyle(Text::Bold);
	text1->setPosition(Vector2f(69, 690.5f));
	text1->setCharacterSize(20);
	text1->setLetterSpacing((float)35.4);
	text1->setString("ABCDEFGH");

	gui::Text* text2 = new gui::Text(*text1);
	text2->setString("HGFEDCBA");
	text2->setPosition(Vector2f(720 - 69, 720 - 690.5f));
	text2->setRotation(180);
	gui::Text* text3 = new gui::Text(*text1);
	text3->setString("8\n7\n6\n5\n4\n3\n2\n1");
	text3->setLineSpacing(3.375);
	text3->setPosition(Vector2f(10, 63));
	gui::Text* text4 = new gui::Text(*text3);
	text4->setString("1\n2\n3\n4\n5\n6\n7\n8");
	text4->setPosition(Vector2f(720 - 10, 720 - 63));
	text4->setRotation(180);

	//fields
	blueTile = gui::RectangleShape(Vector2f(field_size, field_size));
	blueTile.setFillColor(Color::Cyan);
	blueTile.setAlpha(0);

	previous_field = gui::RectangleShape(Vector2f(field_size, field_size));
	previous_field.setFillColor(Color(119, 136, 153));
	previous_field.setAlpha(0);

	current_field = gui::RectangleShape(Vector2f(field_size, field_size));
	current_field.setFillColor(Color(119, 136, 153));
	current_field.setAlpha(0);

	rect_check.setSize(Vector2f(field_size - (float)8.0, field_size - (float)8.0));
	rect_check.setFillColor(Color::Red);
	rect_check.setOrigin(Vector2f(-4, -4));
	rect_check.setAlpha(0);

	possible_field.setSize(Vector2f(field_size - (float)4.0, field_size - (float)4.0));
	possible_field.setFillColor(Color::Color(0, 250, 154));
	possible_field.setOrigin(Vector2f(-2, -2));
	possible_field.setAlpha(0);

	gui::RectangleShape* black_part = new gui::RectangleShape(Vector2f(720, 720));
	black_part->setFillColor(color);
	gui::RectangleShape* white_part = new gui::RectangleShape(Vector2f(field_size * (float)8.1, field_size * (float)8.1));
	white_part->setFillColor(Color::White);
	white_part->setPosition(Vector2f(32, 32));

	parts.push_back(black_part);
	parts.push_back(white_part);
	parts.push_back(text1);
	parts.push_back(text2);
	parts.push_back(text3);
	parts.push_back(text4);

	//checkerboard pattern
	for (int x = 0; x < 8; x++)
	{
		for (int y = 0; y < 8; y++)
		{
			if (((x + 1) % 2 == 0 && (y + 1) % 2 != 0) || ((x + 1) % 2 != 0 && (y + 1) % 2 == 0))
			{
				gui::RectangleShape* black_tile = new gui::RectangleShape(Vector2f(field_size, field_size));
				black_tile->setFillColor(color);
				black_tile->setPosition(Vector2f((float)36.0 + x * field_size, (float)36.0 + y * field_size));
				parts.push_back(black_tile);
			}
		}
	}

	reset_game();
	load_sprites();
}

void Board::reset_game()
{
	//free memory
	if (end)
	{
		for (int x = 0; x < 8; x++)
		{
			for (int y = 0; y < 8; y++)
			{
				if (content[y][x] != NULL)
				{
					delete content[y][x];
				}
			}
		}
		end = false;
	}
	//new content

	content =
	{ {
		{new Castle(false),new Knight(false),new Bishop(false),new Queen(false),new King(false),new Bishop(false),new Knight(false), new Castle(false)},
		{new Pawn(false),  new Pawn(false),	 new Pawn(false),  new Pawn(false),new Pawn(false), new Pawn(false),  new Pawn(false),  new Pawn(false)},
		{    NULL,				NULL,			NULL,				NULL,			NULL,				NULL,				NULL,			NULL},
		{    NULL,				NULL,			NULL,				NULL,			NULL,				NULL,				NULL,			NULL},
		{    NULL,				NULL,			NULL,				NULL,			NULL,				NULL,				NULL,			NULL},
		{    NULL,				NULL,			NULL,				NULL,			NULL,				NULL,				NULL,			NULL},
		{new Pawn(true),   new Pawn(true),   new Pawn(true),   new Pawn(true), new Pawn(true),  new Pawn(true),   new Pawn(true),   new Pawn(true)},
		{new Castle(true), new Knight(true), new Bishop(true), new Queen(true), new King(true), new Bishop(true), new Knight(true), new Castle(true)}
	} };

	previous_field.setAlpha(0);
	current_field.setAlpha(0);
	blueTile.setAlpha(0);
	rect_check.setAlpha(0);

	white_turn = true;
	enemy_on_field = false;
	king_checked = false;
	remis = false;
	check_mated = false;
	possible_fields.clear();
	load_sprites();
	set_textures();
}

boardContent& Board::get_content()
{
	return content;
}

Board::~Board()
{

}

void Board::load_sprites()
{
	//load and store textures
	txt_pawn->loadFromFile("images/pawn.png");
	txt_king->loadFromFile("images/king.png");
	txt_queen->loadFromFile("images/queen.png");
	txt_bishop->loadFromFile("images/bishop.png");
	txt_knight->loadFromFile("images/knight.png");
	txt_castle->loadFromFile("images/castle.png");
	textures.push_back(txt_pawn);
	textures.push_back(txt_king);
	textures.push_back(txt_queen);
	textures.push_back(txt_bishop);
	textures.push_back(txt_knight);
	textures.push_back(txt_castle);

}

void Board::set_textures()
{
	for (auto txt : textures)
	{
		txt->setSmooth(true);
	}

	//set corresponding textures
	for (int x = 0; x < 8; x++)
	{
		for (int y = 0; y < 8; y++)
		{
			Figure* current_figure = content[x][y];

			if (current_figure != NULL)
			{
				if (current_figure->get_type() == PAWN)
				{
					current_figure->setTexture(*txt_pawn, false);
				}
				else if (current_figure->get_type() == KNIGHT)
				{
					current_figure->setTexture(*txt_knight, false);
				}
				else if (current_figure->get_type() == CASTLE)
				{
					current_figure->setTexture(*txt_castle, false);
				}
				else if (current_figure->get_type() == BISHOP)
				{
					current_figure->setTexture(*txt_bishop, false);
				}
				else if (current_figure->get_type() == KING)
				{
					current_figure->setTexture(*txt_king, false);
				}
				else if (current_figure->get_type() == QUEEN)
				{
					current_figure->setTexture(*txt_queen, false);
				}

				//scale the figure to fit
				current_figure->setScale(Vector2f((float)field_size / (float)current_figure->getTextureRect().width,
					(float)field_size / (float)current_figure->getTextureRect().width));

				//set color
				if (!current_figure->check_if_white())
				{
					current_figure->setColor(brown);
				}

				//set positions
				current_figure->setPosition(Vector2f(y*field_size, x*field_size));
			}
		}
	}
}

void Board::drawMe(RenderWindow& window)
{
	for (auto part : parts)
	{
		part->draw(window, position);
	}

	//highlight selected figure
	blueTile.draw(window, position + offset_figure);

	//mark move
	previous_field.draw(window, position + offset_figure);
	current_field.draw(window, position + offset_figure);

	//check
	if (king_checked || remis)
	{
		rect_check.draw(window, position + offset_figure);
	}

	if (!possible_fields.empty())
	{
		for (int i = 0; i < possible_fields.size(); i++)
		{
			possible_fields.at(i).draw(window, position + offset_figure);
		}
	}

	for (int x = 0; x < 8; x++)
	{
		for (int y = 0; y < 8; y++)
		{
			if (content[x][y] != NULL)
			{
				content[x][y]->draw(window, position + offset_figure);
			}
		}
	}
}

void Board::updateMe()
{
	do_moving();
	//updates
	blueTile.update();
	previous_field.update();
	current_field.update();
	rect_check.update();

	for (int x = 0; x < 8; x++)
	{
		for (int y = 0; y < 8; y++)
		{
			if (content[x][y] != NULL)
			{
				content[x][y]->update();
			}
		}
	}

	for (int i = 0; i < possible_fields.size(); i++)
	{
		possible_fields.at(i).update();
	}

}

void Board::event_handling(int mouse_leftClick, Vector2f m_coordinates)
{
	//get coordinates in fields
	Vector2i clicked_field = Vector2i(((int)m_coordinates.x - (int)offset_figure.x) / (const int)field_size, 
		((int)m_coordinates.y - (int)offset_figure.y) / (const int)field_size);

	event_handling(mouse_leftClick, clicked_field);
}

void Board::event_handling(int mouse_leftClick, Vector2i clicked_field)
{
	new_notation = false;
	if (mouse_leftClick == 1 && !end)
	{
		//check if inbounds
		if (clicked_field.x < 8 && clicked_field.y < 8)
		{
			clicked_figure = content[clicked_field.y][clicked_field.x];
			//clicked on a figure
			if (clicked_figure != NULL)
			{
				//my team
				if (clicked_figure->check_if_white() == white_turn)
				{
					//is figure already selected
					if (selected_figure != clicked_figure)
					{
						selected_figure_coordinates = clicked_field;
						selected_figure = clicked_figure;
						blueTile.setPosition(Vector2f(selected_figure_coordinates.x*field_size, selected_figure_coordinates.y*field_size));
						blueTile.setAlpha(0);
						blueTile.fade_in(20);
						current_field.fade_out(20);
						previous_field.fade_out(20);

						destinations = selected_figure->get_possible_destinations(selected_figure_coordinates, content);

						if (!possible_fields.empty())
							possible_fields.clear();

						if (selected_figure->get_type() != KING)
						{
							//correct possible fields
							correct_destinations(destinations);
						}
						else
						{
							vector<Vector2i> copy_of_destinations = destinations;
							correct_destinations(copy_of_destinations);
						}
					}
				}
				else if (selected_figure != NULL)
				{
					check_move(clicked_field, selected_figure, selected_figure_coordinates, destinations);
					move_figure(clicked_field);
				}
			}
			//clicked on a field
			else
			{
				if (selected_figure != NULL)
				{
					check_en_passant(clicked_field, selected_figure, selected_figure_coordinates, destinations);
					check_move(clicked_field, selected_figure, selected_figure_coordinates, destinations);
					move_figure(clicked_field);
				}
			}
		}
	}
}

void Board::check_en_passant(Vector2i clicked_field, Figure* selected_figure, Vector2i selected_coordinates, vector<Vector2i>& destinations)
{
	if (!selected_figure->get_type() == PAWN)
	{
		if (pawn_to_remember != NULL)
			pawn_to_remember->set_en_passant(false);
		return;
	}

	int x = selected_coordinates.x, y = selected_coordinates.y;
	bool is_white = selected_figure->check_if_white();
	int direction = ((Pawn*)selected_figure)->get_direction();

	if (y == 6 && is_white || y == 1 && !is_white)
	{
		if (clicked_field.y == 4 && is_white || clicked_field.y == 3 && !is_white)
		{
			if (pawn_to_remember != NULL)
				pawn_to_remember->set_en_passant(false);

			((Pawn*)selected_figure)->set_en_passant(true);
			pawn_to_remember = (Pawn*)selected_figure;
		}
	}
	else if (pawn_to_remember != NULL)
	{
		pawn_to_remember->set_en_passant(false);
	}
}

void Board::castling(Vector2i clicked_field)
{
	//rochade impossible if moved 
	if (selected_figure->get_type() == KING)
	{
		for (auto rd : castling_destinations)
		{
			if (rd == clicked_field)
			{
				int pos_of_castle_x = 0, pos_of_field_x = 0;
				//set castle positions
				if (clicked_field.x < 4)	//long castling
				{
					pos_of_castle_x = clicked_field.x - 2;
					pos_of_field_x = clicked_field.x + 1;
				}
				else              //short castling
				{
					pos_of_castle_x = clicked_field.x + 1;
					pos_of_field_x = clicked_field.x - 1;
				}
				if (!((King*)selected_figure)->get_moved_already())
				{
					if (!((Castle*)content[selected_figure_coordinates.y][pos_of_castle_x])->get_moved_already())
					{
						content[selected_figure_coordinates.y][pos_of_castle_x]->move_to(Vector2f(pos_of_field_x*field_size, clicked_field.y*field_size), (float)moving_speed, SLOWDOWN);
						swap(content[clicked_field.y][pos_of_castle_x], content[clicked_field.y][pos_of_field_x]);
						if (clicked_field.x < 4)	//left rochade
						{
							short_castling = true;
						}
						else
						{
							long_castling = true;
						}
					}
				}
			}
		}
		((King*)selected_figure)->figure_already_moved();
	}
	else if (selected_figure->get_type() == CASTLE)
	{
		((Castle*)selected_figure)->figure_already_moved();
	}
}

void Board::move_figure(Vector2i clicked_field)
{
	for (auto d : destinations)
	{
		if (clicked_field == d)
		{
			content[selected_figure_coordinates.y][selected_figure_coordinates.x]->move_to(Vector2f(clicked_field.x*field_size, clicked_field.y*field_size), (float)moving_speed, SLOWDOWN);
			if (content[clicked_field.y][clicked_field.x] != NULL)
			{
				enemy_on_field = true;
				delete content[clicked_field.y][clicked_field.x];
				content[clicked_field.y][clicked_field.x] = NULL;
			}
			if (selected_figure->get_type() == PAWN)
			{
				int direction = ((Pawn*)selected_figure)->get_direction();
				if (content[clicked_field.y - direction][clicked_field.x] != NULL)
				{
					if (content[clicked_field.y - direction][clicked_field.x]->check_if_white() != white_turn)
					{
						if (content[clicked_field.y - direction][clicked_field.x]->get_type() == PAWN)
						{
							delete content[clicked_field.y - direction][clicked_field.x];
							content[clicked_field.y - direction][clicked_field.x] = NULL;
							enemy_on_field = true;
						}
					}
				}
			}
			//checks
			castling(clicked_field);
			pawn_promotion(clicked_field);
			swap(content[clicked_field.y][clicked_field.x], content[selected_figure_coordinates.y][selected_figure_coordinates.x]); //move is complete
			previous_field.setPosition(Vector2f(selected_figure_coordinates.x*field_size, selected_figure_coordinates.y*field_size));
			current_field.setPosition(Vector2f(clicked_field.x*field_size, clicked_field.y * field_size));
			notator.set_destinations(destinations);
			finish_turn();
			after_move_checks(clicked_field);
			//notate
			notate(clicked_field, selected_figure, selected_figure_coordinates, content);
			new_notation = true;
		}
	}
}

void Board::pawn_promotion(Vector2i clicked_field)
{
	if (selected_figure->get_type() == PAWN)
	{
		bool is_white = content[selected_figure_coordinates.y][selected_figure_coordinates.x]->check_if_white();
		if (selected_figure_coordinates.y == 1 && is_white || selected_figure_coordinates.y == 6 && !is_white)
		{
			Vector2f originalPosition = content[selected_figure_coordinates.y][selected_figure_coordinates.x]->getPosition();
			Figure* pawn = content[selected_figure_coordinates.y][selected_figure_coordinates.x];
			delete pawn;
			Figure* queen = new Queen(is_white);
			if (selected_figure == pawn)	//set selected figure to queen
				selected_figure = queen;
			content[selected_figure_coordinates.y][selected_figure_coordinates.x] = queen;
			queen->setPosition(originalPosition);
			if (!is_white)
				queen->setColor(brown);
			queen->setTexture(*txt_queen, true);
			queen->setScale(Vector2f((float)field_size / (float)queen->getTextureRect().width,
				(float)field_size / (float)queen->getTextureRect().width));
			queen->move_to(Vector2f(clicked_field.x*field_size, clicked_field.y*field_size), (float)moving_speed, SLOWDOWN);
			promotion = true;
		}
	}
}

void Board::finish_turn()
{
	white_turn = !white_turn;
	destinations.clear();
	possible_fields.clear();
	previous_field.fade_in(40);
	blueTile.fade_out(40);
	current_field.fade_in(40);
}

void Board::after_move_checks(Vector2i clicked_field)
{
	king_checked = false;	//king must move if check or must be saved
	rect_check.fade_out(40);
	vector<Vector2i> enemy_destinations = get_destinations_of_all_figures(white_turn, true);

	for (int x = 0; x < 8; x++)
	{
		for (int y = 0; y < 8; y++)
		{
			if (content[y][x] != NULL && content[y][x]->get_type() == KING)
			{
				if (content[y][x]->check_if_white() == white_turn)
				{
					//check if king checked
					for (auto dfc : enemy_destinations)
					{
						if (dfc == Vector2i(x, y))
						{
							king_checked = true;
							((King*)content[y][x])->set_checked(true);
							rect_check.setPosition(Vector2f(x*field_size, y*field_size));
							rect_check.fade_in(40);
							break;
						}
						else
						{
							((King*)content[y][x])->set_checked(false);
							king_checked = false;
						}
					}
					//check if checkmated
					if (king_checked)
					{
						vector<Vector2i> king_destinations = content[y][x]->get_possible_destinations(Vector2i(x, y), content);
						if (!check_if_king_can_move(x, y, king_destinations))
						{
							vector<Vector2i> destinations_of_my_figures = get_destinations_of_all_figures(!white_turn, false);
							if (destinations_of_my_figures.empty())
							{
								check_mated = true;
								rect_check.setFillColor(Color::Magenta);
							}
						}
					}
					else	// check if remis
					{
						vector<Vector2i> destinations_of_my_figures = get_destinations_of_all_figures(!white_turn, false);
						if (destinations_of_my_figures.empty())
						{
							vector<Vector2i> king_destinations = content[y][x]->get_possible_destinations(Vector2i(x, y), content);
							if (!check_if_king_can_move(x, y, king_destinations))
							{
								remis = true;
								rect_check.setPosition(Vector2f(x*field_size, y*field_size));
								rect_check.setFillColor(Color::Yellow);
							}
						}
					}
				}
			}
		}
	}
}

bool Board::check_if_king_can_move(int x, int y, vector<Vector2i>& king_destinations)
{
	for (int m = -1; m < 2; m++)
	{
		for (int n = -1; n < 2; n++)
		{
			if (!(m == 0 && n == 0) && (0 <= x + m < 8) && (0 <= y + n < 8))
			{
				check_move(Vector2i(x + m, y + n), content[y][x], Vector2i(x, y), king_destinations);
			}
		}
	}
	if (king_destinations.empty())
	{
		return false;
	}
	return true;
}

void Board::check_move(Vector2i field_to_check, Figure* figure_to_check, Vector2i selected_coordinates, vector<Vector2i>& destinations)
{
	Figure* clicked_figure = NULL;
	for (auto d : destinations)
	{
		if (field_to_check == d)
		{
			//able to eat enemies
			if (content[field_to_check.y][field_to_check.x] != NULL)	
			{
				//remember
				clicked_figure = content[field_to_check.y][field_to_check.x];	
				content[field_to_check.y][field_to_check.x] = NULL;
			}
			//swap to check if move is possible
			swap(content[field_to_check.y][field_to_check.x], content[selected_coordinates.y][selected_coordinates.x]);

			//king has other logic
			if (figure_to_check->get_type() != KING)	
			{
				for (int x = 0; x < 8; x++)
				{
					for (int y = 0; y < 8; y++)
					{
						if (content[y][x] != NULL)
						{
							//delete clicked_field if king is in danger after move 
							if (content[y][x]->get_type() == KING && content[y][x]->check_if_white() == white_turn)
							{
								delete_impossible_destinations(field_to_check, Vector2i(x, y), selected_coordinates, clicked_figure, destinations);
							}
						}
					}
				}
			}
			else
			{
				delete_impossible_destinations(field_to_check, field_to_check, selected_coordinates, clicked_figure, destinations);
			}
		}
	}
}

void Board::delete_impossible_destinations(Vector2i field_to_check, Vector2i new_king_coordinates, Vector2i selected_coordinates, Figure* clicked_figure, vector<Vector2i>& destinations)
{
	vector<Vector2i> enemy_destinations = get_destinations_of_all_figures(white_turn, true);
	//king, extra rule for rochade
	if (new_king_coordinates == field_to_check)	
	{
		for (int x = -1; x < 2; x += 2)
		{
			if (field_to_check.x == selected_coordinates.x + (x * 2))
			{
				for (auto ed : enemy_destinations)
				{
					if (ed == Vector2i(selected_coordinates.x + x, selected_coordinates.y))
					{
						for (int i = 0; i < destinations.size(); i++)
						{
							if (destinations.at(i) == Vector2i(selected_coordinates.x + (x * 2), selected_coordinates.y))
							{
								destinations.erase(destinations.begin() + i);
							}
						}
					}
				}
			}
		}
	}
	//check if king would be in danger 
	for (auto ed : enemy_destinations)
	{
		if (ed == new_king_coordinates)
		{
			for (int i = 0; i < destinations.size(); i++)
			{
				if (destinations.at(i) == field_to_check)
				{
					//if king in danger, figure must not move
					destinations.erase(destinations.begin() + i);	
				}
			}
		}
	}
	//swap to the previous position
	if (clicked_figure != NULL)
	{
		swap(content[field_to_check.y][field_to_check.x], content[selected_coordinates.y][selected_coordinates.x]);

		//place back previous figure
		content[field_to_check.y][field_to_check.x] = clicked_figure;
		clicked_figure->move_to(Vector2f(field_to_check.x * field_size, field_to_check.y * field_size), (float)moving_speed, SLOWDOWN);
	}
	else
	{
		swap(content[field_to_check.y][field_to_check.x], content[selected_coordinates.y][selected_coordinates.x]);
	}
}

vector<Vector2i> Board::get_destinations_of_all_figures(bool white_turn, bool enemy)
{
	vector<Vector2i> destinations_of_all_figures;
	for (int x = 0; x < 8; x++)
	{
		for (int y = 0; y < 8; y++)
		{
			if (content[y][x] != NULL)
			{
				//enemy
				if (content[y][x]->check_if_white() != white_turn)	
				{
					if (!enemy && content[y][x]->get_type() != KING)
					{
						vector<Vector2i> destinatations_to_check = content[y][x]->get_possible_destinations(Vector2i(x, y), content);
						for (auto dtc : destinatations_to_check)
						{
							destinations_of_all_figures.push_back(dtc);
							check_move(dtc, content[y][x], Vector2i(x, y), destinations_of_all_figures);
						}
					}
					else if (enemy)
					{
						vector<Vector2i> destinatations_to_check = content[y][x]->get_possible_destinations(Vector2i(x, y), content);
						if (!destinatations_to_check.empty())
						{
							for (auto dtc : destinatations_to_check)
							{
								destinations_of_all_figures.push_back(dtc);
							}
						}
					}
				}
			}
		}
	}
	return destinations_of_all_figures;
}

void Board::correct_destinations(vector<Vector2i>& destinations)
{
	possible_fields.clear();

	if (selected_figure->get_type() == KING)
	{
		for (int x = -1; x < 2; x++)
		{
			for (int y = -1; y < 2; y++)
			{
				if (!(x == 0 && y == 0))
				{
					check_move(Vector2i(selected_figure_coordinates.x + x, selected_figure_coordinates.y + y), selected_figure, selected_figure_coordinates, destinations);
				}
			}
		}
	}
	else
	{
		std::vector<Vector2i> tempDestinations = destinations;

		for (const auto dest: tempDestinations)
		{
			check_move(dest, selected_figure, selected_figure_coordinates, destinations);
		}
	}
	//correct possible fields
	for (int i = 0; i < destinations.size(); i++)
	{
		possible_field.setPosition(Vector2f(destinations.at(i).x * field_size, destinations.at(i).y * field_size));
		possible_field.setAlpha(200);
		possible_fields.push_back(possible_field);
		possible_fields.at(i).fade_in(20);
	}
}

void Board::notate(Vector2i clicked_field, Figure* selected_figure, Vector2i selected_coordinates, boardContent& content)
{
	notator.set_booleans(white_turn, enemy_on_field, promotion, long_castling, short_castling, end);
	if (check_mated || remis)
	{
		finish_turn();
	}
	if (!long_castling && !short_castling)
	{
		if (promotion)
		{
			promotion = false;
			notator.set_string(notator_strings.at(PROMOTION));
		}
		else if (remis)
		{
			notator.set_string(notator_strings.at(REMIS));
			end = true;
			notator.set_booleans(white_turn, enemy_on_field, promotion, long_castling, short_castling, end);
			notator.write_data(clicked_field, selected_figure, selected_coordinates, content, notation);
		}
		else if (check_mated)
		{
			notator.set_string(notator_strings.at(CHECKMATED));
			notator.write_data(clicked_field, selected_figure, selected_coordinates, content, notation);
			end = true;
			if (white_turn)
			{
				notator.set_string(notator_strings.at(WHITEWINS));
			}
			else
			{
				notator.set_string(notator_strings.at(BLACKWINS));
			}
			notator.set_booleans(white_turn, enemy_on_field, promotion, long_castling, short_castling, end);
			notator.write_data(clicked_field, selected_figure, selected_coordinates, content, notation);
		}
		else if (king_checked)
		{
			notator.set_string(notator_strings.at(CHECKANDCHECKMATED));
		}
		if (enemy_on_field)
			enemy_on_field = false;
	}
	else if (long_castling)
	{
		notator.set_string(notator_strings.at(LEFT));
		long_castling = false;
	}
	else if (short_castling)
	{
		notator.set_string(notator_strings.at(RIGHT));
		short_castling = false;
	}

	if (!end)
		notator.write_data(clicked_field, selected_figure, selected_coordinates, content, notation);
}

bool Board::new_notation_available()
{
	if (new_notation)
	{
		new_notation = false;
		return true;
	}
	return false;
}

vector<string>* Board::get_notation()
{
	return &notation;
}

void Board::fadeInFigures()
{
	for (int x = 0; x < 8; x++)
	{
		for (int y = 0; y < 8; y++)
		{
			if (content[x][y] != NULL)
			{
				content[x][y]->fade_in(15);
			}
		}
	}
}

void Board::fadeOutFigures()
{
	for (int x = 0; x < 8; x++)
	{
		for (int y = 0; y < 8; y++)
		{
			if (content[x][y] != NULL)
			{
				content[x][y]->fade_out(15);
			}
		}
	}
}

void Board::set_content(boardContent content)
{
	this->content = content;
}