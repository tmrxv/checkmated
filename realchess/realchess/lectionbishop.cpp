#include "LectionBishop.h"


LectionBishop::LectionBishop(string name)
{
	this->name = name;
	font.loadFromFile("C:/Windows/Fonts/calibri.ttf");

	positions =
	{ {
		Vector2i(4, 6), { 4, 4 }, Vector2i(4, 1), { 4, 2 },
		Vector2i(5, 7), { 5, 7 }, Vector2i(5, 7), { 1, 3 }, Vector2i(5, 0), { 2, 3 },
		Vector2i(1, 3), { 1, 3 }, Vector2i(1, 3), { 3, 1 }, Vector2i(2, 0), { 3, 1 }
	} };

	stdContent = true;
	textOrder = { 2, 3, 5, 6, 8 };
}

LectionBishop::LectionBishop()
{

}

LectionBishop::~LectionBishop()
{

}

void LectionBishop::event_handling(Board* board, int mouse_click)
{
	show_text(mouse_click);

	if (mouse_click == 1)
	{
		if (move <= positions.size())
		{
			move_figure(board, mouse_click, positions[move - 1][0], positions[move - 1][1]); //-1 because move starts at 1
		}
		else
		{
			boardInteractable = true;
		}
	}

}