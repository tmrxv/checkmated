#include "container.h"

Container::Container()
{
}

Container::~Container()
{
}

Container::Container(Vector2f position)
{
	this->position = position;
	this->destination_position = position;
}

void Container::draw(RenderWindow& window, Vector2f offset)
{
	for (auto component : components)
	{
		component->draw(window, Vector2f(position.x + offset.x, position.y + offset.y));
	}
	for (auto object : objects)
	{
		object->draw(window, Vector2f(position.x + offset.x, position.y + offset.y));
	}
}

void Container::draw(RenderWindow& window)
{
	draw(window, Vector2f(0, 0));
}

void Container::push_back(Component* component)
{
	components.push_back(component);
}

void Container::push_back(GraphicalObject* object)
{
	objects.push_back(object);
}

void Container::push_back(vector<gui::Sprite*> objects)
{
	for (auto object : objects)
	{
		this->objects.push_back(object);
	}
}

void Container::update()
{
	do_moving();
}

void Container::push_back(vector<gui::RectangleShape*> objects)
{
	for (auto object : objects)
	{
		this->objects.push_back(object);
	}
}