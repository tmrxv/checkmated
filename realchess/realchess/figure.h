#pragma once
#include <SFML/Graphics.hpp>
#include "sprite.h"
#include <array>

using namespace std;
using namespace sf;

class Figure;
typedef array<array<Figure*, 8>, 8> boardContent;

enum FigureType
{
	PAWN, KNIGHT, CASTLE, BISHOP, KING, QUEEN
};

class Figure : public gui::Sprite
{
protected:
	//variables
	FigureType type;
	bool is_white;

	//methods
	bool in_bounds(int, int);

public:
	//con-/destructor
	Figure();
	virtual ~Figure();

	//methods
	virtual vector<Vector2i> get_possible_destinations(Vector2i, boardContent&) = 0;
	bool check_if_white();
	FigureType get_type();
};