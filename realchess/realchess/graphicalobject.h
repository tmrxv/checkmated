#pragma once
#include <SFML/Graphics.hpp>
#include "fadable.h"
#include "movable.h"

using namespace sf;

class GraphicalObject : public Fadable, public Movable
{
private:
	//functions
	virtual void setAlphas() = 0;
public:
	GraphicalObject();
	virtual ~GraphicalObject();

	virtual void draw(RenderWindow&, Vector2f) = 0;
	virtual void draw(RenderWindow&) = 0;
};