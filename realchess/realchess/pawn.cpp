#include "pawn.h"

Pawn::Pawn(bool is_white)
{
	type = PAWN;
	this->is_white = is_white;
	this->en_passant = false;

	if (is_white)
	{
		direction = -1;
	}
	else
	{
		direction = 1;
	}
}

Pawn::~Pawn()
{

}

vector<Vector2i> Pawn::get_possible_destinations(Vector2i my, boardContent& content)
{
	vector<Vector2i> possible_destinations;
	bool move_two_steps = false;

	if (my.y == 1 && !is_white || my.y == 6 && is_white)
	{
		move_two_steps = true;
	}
	for (int x = -1; x < 2; x++)
	{
		int y = 0;
		do
		{
			//check if in bounds
			if (in_bounds(my.x + x, my.y + y))
			{
				//fields on the side
				if (x != 0)
				{
					//fields right next to me
					if (y == 0)
					{
						if (content[my.y + y][my.x + x] != NULL)
						{
							if (content[my.y + y][my.x + x]->check_if_white() != is_white)
							{
								if (content[my.y + y][my.x + x]->get_type() == PAWN)
								{
									if (((Pawn*)content[my.y + y][my.x + x])->get_en_passant())
									{
										possible_destinations.push_back(Vector2i(my.x + x, my.y + direction));
									}
								}
							}
						}
					}
					//diagonal fields
					else
					{
						if (content[my.y + y][my.x + x] != NULL)
						{
							if (content[my.y + y][my.x + x]->check_if_white() != is_white)
							{
								possible_destinations.push_back(Vector2i(my.x + x, my.y + y));
							}
						}
					}
				}
			}
			y += direction;
		} while (y == direction);
	}
	//fields above/below
	if (content[my.y + direction][my.x] == NULL)
	{
		possible_destinations.push_back(Vector2i(my.x, my.y + direction));
		//second field empty
		if (move_two_steps && content[my.y + 2 * direction][my.x] == NULL)
		{
			possible_destinations.push_back(Vector2i(my.x, my.y + (2 * direction)));
		}
	}

	return possible_destinations;
}

int Pawn::get_direction()
{
	return direction;
}

bool Pawn::get_en_passant()
{
	return en_passant;
}
void Pawn::set_en_passant(bool value)
{
	en_passant = value;
}