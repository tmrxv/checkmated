#include "LectionKnight.h"

LectionKnight::LectionKnight(string name)
{
	this->name = name;
	font.loadFromFile("C:/Windows/Fonts/calibri.ttf");

	positions =
	{ {
		Vector2i(1, 0), { 1, 0 }, Vector2i(1, 0), { 2, 2 }, Vector2i(6, 7), { 5, 5 },
		Vector2i(2, 2), { 3, 4 }, Vector2i(5, 5), { 7, 4 },
		Vector2i(3, 4), { 3, 4 }, Vector2i(3, 4), { 5, 3 }, Vector2i(1, 7), { 2, 5 },
		Vector2i(5, 3), { 3, 4 }, Vector2i(2, 5), { 1, 3 },
		Vector2i(3, 4), { 3, 4 }, Vector2i(3, 4), { 2, 6 }
	} };

	stdContent = true;
}

LectionKnight::LectionKnight()
{

}

LectionKnight::~LectionKnight()
{

}

void LectionKnight::event_handling(Board* board, int mouse_click)
{
	show_text(mouse_click);

	if (mouse_click == 1)
	{
		if (move <= positions.size())
		{
			if (move == 1 || move == 3 || move == 6 || move == 11 || move == 12)
			{
				showText = true;
			}

			move_figure(board, mouse_click, positions[move - 1][0], positions[move - 1][1]);
		}
		else
		{
			boardInteractable = true;
		}
	}
}