#pragma once
#include <SFML/Graphics.hpp>

using namespace sf;
using namespace std;

namespace my
{
	class Clock
	{
	private:
		sf::Clock clock;
		Int64 stored_time = 0;
		bool paused = false;
		bool countUpwards = true;
		Int64 seconds;
		Int64 minutes;
		Int64 hours;
	public:
		Clock();
		Clock(int, bool);
		virtual ~Clock();
		void update();
		void pause();
		void resume();
		string getSeconds();
		string getMinutes();
		string getHours();
	};
}