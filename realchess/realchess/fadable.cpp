#include "fadable.h"

Fadable::Fadable()
{
}

Fadable::~Fadable()
{
}

void Fadable::do_fading()
{
	//alpha
	if (fading)
	{
		if (alpha < destination_alpha)
		{
			alpha += speed_alpha;
		}
		if (alpha > destination_alpha)
		{
			alpha = destination_alpha;
		}
	}
	else
	{
		if (alpha > destination_alpha)
		{
			alpha = alpha - speed_alpha;
		}
		if (alpha < destination_alpha)
		{
			alpha = destination_alpha;
		}
	}

	//set all alpha values
	setAlphas();
}

void Fadable::fade_in(int speed)
{
	destination_alpha = 255;
	speed_alpha = speed;
	fading = true;
}

void Fadable::fade_out(int speed)
{
	destination_alpha = 0;
	speed_alpha = speed;
	fading = false;
}

void Fadable::setAlpha(int alpha)
{
	this->alpha = alpha;
	this->destination_alpha = alpha;
	setAlphas();
}

void Fadable::fade_to(int destination_alpha, int speed_alpha)
{
	this->destination_alpha = destination_alpha;
	this->speed_alpha = speed_alpha;
	if (destination_alpha < alpha)
	{
		fading = false;
	}
	else
	{
		fading = true;
	}
}
void Fadable::setEffectedByGlobalAlpha(bool effectedByGlobalAlpha)
{
	this->effectedByGlobalAlpha = effectedByGlobalAlpha;
}

bool Fadable::isEffectedByGlobalAlpha()
{
	return effectedByGlobalAlpha;
}