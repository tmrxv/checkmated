#include "Lection.h"

Lection::Lection(string name)
{
	this->name = name;
	this->content = { {} };
}

Lection::Lection()
{
}

Lection::~Lection()
{
}

boardContent& Lection::get_content()
{
	return content;
}

bool Lection::check_if_content_is_std()
{
	if (stdContent)
	{
		stdContent = false;
		return true;
	}
	return false;
}

string Lection::get_name()
{
	return name;
}

bool Lection::showInformation()
{
	if (showText)
	{
		showText = false;
		return true;
	}
	else
	{
		return false;
	}
}

void Lection::show_text(int mouse_click)
{
	if (move == 0)
	{
		showText = true;
		move++;
	}
	else if (mouse_click == 1)
	{
		for (int i = 0; i < textOrder.size(); i++)
		{
			if (move == textOrder.at(i))
			{
				showText = true;
				break;
			}
		}
	}
}

bool Lection::boardIsInteractable()
{
	if (boardInteractable)
	{
		boardInteractable = false;
		return true;
	}
	else
	{
		return false;
	}
}

vector<gui::Text> Lection::get_lection_infos()
{
	name += ".txt";
	const char* c_name = name.c_str();

	file = fopen(c_name, "rt");
	if (file == NULL)
	{
		fprintf(stderr, "Lection::Error, could not open txtFile\n");
		exit(1); //error
	}
	else
	{
		fprintf(stderr, "Successfully entered txtFile from Lection\n");
		fgets(input, sizeof(input), file);
		nLines = atoi(input);
		// show notation at the end
		for (int i = 0; i < nLines; i++)
		{
			fgets(input, sizeof(input), file);
			ptr = strtok(input, "*");
			gui::Text text = gui::Text(font, Color::White, 22);
			text.setString(ptr);
			lectionInfos.push_back(text);
		}
	}
	fclose(file);
	return lectionInfos;
}

void Lection::move_figure(Board* board, int mouse_click, Vector2i start, Vector2i end)
{
	board->event_handling(mouse_click, start);
	board->event_handling(mouse_click, end);
	move++;
}
