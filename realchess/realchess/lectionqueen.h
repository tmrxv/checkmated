#pragma once
#include "Lection.h"
class LectionQueen :
	public Lection
{
private:
	vector<vector<Vector2i>> positions;
public:
	LectionQueen();
	LectionQueen(string);
	virtual ~LectionQueen();
	virtual void event_handling(Board*, int);
};

