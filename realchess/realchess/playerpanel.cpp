#include "playerpanel.h"
#include <iostream>

PlayerPanel::PlayerPanel()
{

}

PlayerPanel::PlayerPanel(Font& font, bool upsideDown, GUI* gui, Board* board)
{
	this->board = board;
	this->gui = gui;
	nameLabel = new gui::Sprite("images/gui/chess/name_label.png");
	watch = new gui::Sprite("images/gui/chess/watch.png", Vector2f(23, 447));

	playerName = Label(Color::Transparent, Color::Black, Vector2f(0, 0), Vector2f(nameLabel->getLocalBounds().width, nameLabel->getLocalBounds().height),
		"Spieler", font, 0, false);
	//color
	Color grey = Color(72, 72, 72, 255);
	Color silver = Color(87, 87, 87, 255);

	clock = Label(Color::Transparent, grey, Vector2f(81, 447), Vector2f(288, 58), "00:00:00", font, 0, false);
	pane.setSize(Vector2f(528, 624));
	pane.setFillColor(Color(40, 40, 40, 255));
	pane.setPosition(Vector2f(0, nameLabel->getLocalBounds().height));

	for (auto i = 0; i < 6; i++)
	{
		if (i % 2 != 0)
		{
			labels.push_back(new Label(grey, Color::White, Vector2f(23, 89 + i * 58), Vector2f(482, 58), "XXXXXXXXX", font, -1, false));
		}
		else
		{
			labels.push_back(new Label(silver, Color::White, Vector2f(23, 89 + i * 58), Vector2f(482, 58), "XXXXXXXXX", font, -1, false));
		}
	}

	for (auto i = 0; i < 4; i++)
	{
		Component* comp = new Component();
		comp->push_back(new gui::RectangleShape(Vector2f(113, 80)));
		comp->setPosition(Vector2f(23, 599) + Vector2f(i*(10 + 113), 0));
		buttons.push_back(FloatRect(Vector2f(23, 599) + Vector2f(i*(10 + 113), 0), Vector2f(113, 80)));
		switch (i)
		{
		case 0:	comp->push_back(new gui::Sprite("images/gui/chess/icon_pause.png")); break;
		case 1: comp->push_back(new gui::Sprite("images/gui/chess/icon_reset.png")); break;
		case 2: comp->push_back(new gui::Sprite("images/gui/chess/icon_save.png")); break;
		case 3: comp->push_back(new gui::Sprite("images/gui/chess/icon_back.png")); break;
		}
		push_back(comp);
	}

	//pausescreen
	blackscreen = gui::RectangleShape(Vector2f(1280, 720));
	blackscreen.setFillColor(Color::Black);
	blackscreen.setAlpha(0);
	pauseText = gui::Text(font, Color::White, 90);
	pauseText.setString("Pausiert");
	pauseText.setOrigin(Vector2f(pauseText.getLocalBounds().left + pauseText.getLocalBounds().width / 2,
		pauseText.getLocalBounds().top + pauseText.getLocalBounds().height / 2));
	pauseText.setAlpha(0);
	pauseInfo = gui::Text(font, Color(155, 155, 155, 255), 55);
	pauseInfo.setString("Zum Fortsetzen klicken");
	pauseInfo.setOrigin(Vector2f(pauseInfo.getLocalBounds().left + pauseInfo.getLocalBounds().width / 2,
		pauseInfo.getLocalBounds().top + pauseInfo.getLocalBounds().height / 2));
	pauseInfo.setAlpha(0);
	pauseInfo.setPosition(Vector2f(640, 400));

	yes = new gui::Sprite("images/gui/chess/yes.png", Vector2f(741, 360));
	no = new gui::Sprite("images/gui/chess/no.png", Vector2f(239, 360));
	yes->setAlpha(0);
	no->setAlpha(0);

	push_back(&this->clock);
	push_back(&this->playerName);
	push_back(&this->pane);
	push_back(nameLabel);
	push_back(watch);
}

PlayerPanel::~PlayerPanel()
{
	delete nameLabel->getTexture();
	delete watch->getTexture();
	delete nameLabel;
	delete watch;

	delete yes->getTexture();
	delete no->getTexture();
	delete yes;
	delete no;
}

void PlayerPanel::drawMe(RenderWindow& window)
{
	for (auto object : objects)
	{
		object->draw(window, position);
	}

	for (auto comp : components)
	{
		comp->draw(window, position);
	}

	for (auto label : labels)
	{
		label->draw(window, position);
	}

	blackscreen.draw(window);
	pauseText.draw(window);
	pauseInfo.draw(window);
	yes->draw(window);
	no->draw(window);
}

void PlayerPanel::addNotation(vector<string>* notation)
{
	int notationLabel = currentLabel;
	if (notationLabel == 6)
	{
		notationFull = true;
	}
	if (notationFull)
	{
		notationLabel = 5;
	}
	if (firstPart)
	{
		if (notationFull)
		{
			for (int i = 0; i < 5; i++)
			{
				labels.at(i)->set_string(labels.at(i + 1)->get_string());
			}
		}
		labels.at(notationLabel)->set_string(to_string(currentLabel + 1) + ". " + notation->back());
		firstPart = false;
	}
	else
	{
		firstPart = true;
		labels.at(notationLabel)->set_string(to_string(currentLabel + 1) + ". " + notation->at(notation->size() - 2) + " " + notation->back());
		currentLabel++;
	}
}

void PlayerPanel::event_handling(int mouse_leftClick, Vector2f mouse_coordinates)
{
	if (inputAllowed)
	{
		startTimer();
		if (board->is_triggered())
		{
			gui->setBoardInteractable(true);
		}
		else if (mouse_leftClick == 1)
		{
			if (state == PLAYING)
			{
				for (int i = 0; i < buttons.size(); i++)
				{
					if (buttons.at(i).contains(mouse_coordinates - position))
					{
						if (i == 0)
						{
							state = PAUSED;
							openPauseScreen();
						}
						else if (i == 1)
						{
							state = RESET;
							openPauseScreen();
						}
						else if (i == 2)
						{
							state = SAVE;
							openPauseScreen();
						}
						else if (i == 3)
						{
							state = EXIT;
							openPauseScreen();
						}
					}
				}
			}
			else if (state == PAUSED)
			{
				closePauseScreen();
				state = PLAYING;
			}
			else
			{
				if (no->getGlobalBounds().contains(mouse_coordinates))
				{
					closePauseScreen();
					state = PLAYING;
				}
				else if (yes->getGlobalBounds().contains(mouse_coordinates))
				{
					if (state == RESET)
					{
						closePauseScreen();
						inputAllowed = false;
						gui->hideBoard();
						firstPart = false;
					}
				}
			}
		}
	}
	else if (state == RESET && board->is_triggered())
	{
		board->reset_game();
		gui->showBoard();
		inputAllowed = true;
		state = PLAYING;
	}

	blackscreen.update();
	pauseText.update();
	pauseInfo.update();
	yes->update();
	no->update();
}

void PlayerPanel::openPauseScreen()
{
	blackscreen.fade_to(215, 18);
	pauseText.fade_in(15);
	gui->setBoardInteractable(false);
	board->fadeOutFigures();
	if (state == PAUSED)
	{
		pauseText.setString("Pausiert");
		pauseText.setStyle(!Text::Underlined);
		pauseText.setPosition(Vector2f(640, 319));
		pauseInfo.fade_in(15);
	}
	else
	{
		yes->fade_in(15);
		no->fade_in(15);
		pauseText.setStyle(Text::Underlined);
		pauseText.setPosition(Vector2f(640, 290));
		if (state == RESET)
		{
			pauseText.setString("Aktuelle Partie neustarten?");
		}
		else if (state == SAVE)
		{
			pauseText.setString("Partie speichern und beenden?");
		}
		else if (state == EXIT)
		{
			pauseText.setString("Partie ohne speichern beenden?");
		}
	}

	pauseText.setOrigin(Vector2f(pauseText.getLocalBounds().left + pauseText.getLocalBounds().width / 2,
		pauseText.getLocalBounds().top + pauseText.getLocalBounds().height / 2));
}

void PlayerPanel::closePauseScreen()
{
	yes->fade_out(18);
	no->fade_out(18);
	blackscreen.fade_out(15);
	pauseText.fade_out(18);
	pauseInfo.fade_out(18);
	board->fadeInFigures();
	gui->setBoardInteractable(true);
}

void PlayerPanel::startTimer()
{
	if (second.count() < 60)
	{
		second += chrono::seconds(1);
	}
	else if (second.count() == 60)
	{
		minute += chrono::minutes(1);
		second = chrono::seconds(0);
	}
	else if (minute.count() == 60)
	{
		hour += chrono::hours(1);
	}
}