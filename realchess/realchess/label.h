#pragma once
#include "component.h"
#include "rectangleshape.h"
#include "text.h"

class Label : public Component
{
private:
	gui::RectangleShape* bounds;
	gui::Text* text;

	Color color_bounds;
	Color color_text;
	int alignment = -1;
	bool upside_down = false;
public:
	Label();
	Label(Color, Color, Vector2f, Vector2f, string, Font&, int, bool);
	virtual ~Label();

	void set_string(string);
	string get_string();
	void set_text_alignment();
	void event_handling(int, Vector2i);
};