#include "LectionKing.h"

LectionKing::LectionKing(string name)
{
	this->name = name;
	font.loadFromFile("C:/Windows/Fonts/calibri.ttf");

	positions =
	{ {
		Vector2i(2, 6), { 2, 5 }, Vector2i(5, 1), { 5, 2 },
		Vector2i(3, 7), { 2, 6 }, Vector2i(4, 0), { 5, 1 },
		Vector2i(6, 7), { 7, 5 }, Vector2i(5, 1), { 4, 2 },
		Vector2i(4, 6), { 4, 5 }, Vector2i(4, 2), { 4, 3 },
		Vector2i(7, 5), { 6, 3 }, Vector2i(4, 3), { 4, 3 }, Vector2i(4, 3), { 3, 3 }
	} };

	stdContent = true;
}

LectionKing::LectionKing()
{

}

LectionKing::~LectionKing()
{

}

void LectionKing::event_handling(Board* board, int mouse_click)
{
	show_text(mouse_click);

	if (mouse_click == 1)
	{
		if (move <= positions.size())
		{
			if (move == 3 || move == 9 || move == 11)
			{
				showText = true;
			}

			move_figure(board, mouse_click, positions[move - 1][0], positions[move - 1][1]);
		}
		else
		{
			boardInteractable = true;
		}
	}
}