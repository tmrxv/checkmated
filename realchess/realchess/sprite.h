#pragma once
#include "graphicalobject.h"

namespace gui
{
	class Sprite : public GraphicalObject
	{
	private:
		sf::Sprite spr;

		//functions
		void setAlphas();

	public:
		Sprite();
		Sprite(string);
		Sprite(string, Vector2f);
		virtual ~Sprite();

		//adapter functions
		IntRect getTextureRect();
		void setScale(Vector2f);
		void setColor(Color);
		void setTexture(Texture&, bool);
		FloatRect getLocalBounds();
		FloatRect getGlobalBounds();
		Color getColor();

		//functions
		void draw(RenderWindow&, Vector2f);
		void draw(RenderWindow&);
		void update();
		const Texture* getTexture();
	};
}