#include "knight.h"

Knight::Knight(bool is_white)
{
	type = KNIGHT;
	this->is_white = is_white;
}

Knight::~Knight()
{

}

vector<Vector2i> Knight::get_possible_destinations(Vector2i my, boardContent& content)
{
	vector<Vector2i> possible_destinations;
	int absoluteValueX = 0;
	int absoluteValueY = 0;

	for (int x = -2; x <=2; x++)
	{
		for (int y = -2; y <=2; y++)
		{
			if (x < 0)
			{
				absoluteValueX = -x;
			}
			else
			{
				absoluteValueX = x;
			}
			if (y < 0)
			{
				absoluteValueY = -y;
			}
			else
			{
				absoluteValueY = y;
			}
			if ((absoluteValueX == 1 && absoluteValueY == 2) || (absoluteValueX == 2 && absoluteValueY == 1))
			{
				if (in_bounds(my.y + y, my.x + x))
				{
					//figure
					if (content[my.y + y][my.x + x] != NULL)
					{
						//enemy
						if (content[my.y + y][my.x + x]->check_if_white() != is_white)
						{
							possible_destinations.push_back(Vector2i(my.x + x, my.y + y));
						}
					}
					//empty
					else
					{
						possible_destinations.push_back(Vector2i(my.x + x, my.y + y));
					}
				}
			}
		}
	}
	return possible_destinations;
}