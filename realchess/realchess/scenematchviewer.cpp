#include "scenematchviewer.h"

SceneMatchViewer::SceneMatchViewer()
{

}

SceneMatchViewer::~SceneMatchViewer()
{
}

SceneMatchViewer::SceneMatchViewer(GUI* gui, Board* board) : Scene(gui)
{
	this->board = board;

	datax = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h' };
	datay = { '8', '7', '6', '5', '4', '3', '2', '1' };
	first_letters = { 'N', 'R', 'B', 'K', 'Q' };
	figure_types = { KNIGHT, CASTLE, BISHOP, KING, QUEEN };

	startup();
	readPGNFile();
}

void SceneMatchViewer::draw(RenderWindow& window)
{

}

void SceneMatchViewer::startup()
{
	gui->showBoard();
}

void SceneMatchViewer::event_handling(int left_mouse_click, Vector2f m_coordinates_fields)
{
	if (state == STARTING)
	{
		if (board->is_triggered())
		{
			state = RUNNING;
		}
	}
	else if (state == RUNNING)
	{
		if (left_mouse_click == 1)
		{
			removeExtraLetters();
			for (int i = 0; i < first_letters.size(); i++)
			{
				//check figure type
				if (notation.at(moveNr).at(0) == first_letters.at(i))
				{
					if (notation.at(moveNr).size() == 3)
					{
						int x = translateCoordinate(notation.at(moveNr).at(1));
						int y = translateCoordinate(notation.at(moveNr).at(2));
						Vector2i figureCoordinates = searchFigure(figure_types.at(i), Vector2i(x, y));
						board->event_handling(1, figureCoordinates);
						board->event_handling(1, Vector2i(x, y));
					}
					else
					{
						int x = translateCoordinate(notation.at(moveNr).at(2));
						int y = translateCoordinate(notation.at(moveNr).at(3));
						Vector2i figureCoordinates = searchFigure(figure_types.at(i), Vector2i(x, y), notation.at(moveNr).at(1));
						board->event_handling(1, figureCoordinates);
						board->event_handling(1, Vector2i(x, y));
					}
					moveNr++;
					whiteTurn = !whiteTurn;
					return;
				}
			}
			//check for castling
			if (notation.at(moveNr).at(0) == 'O')
			{
				//long castling
				int y = (whiteTurn) ? 7 : 0;
				if (notation.at(moveNr).size() > 3)
				{
					board->event_handling(1, Vector2i(4, y));
					board->event_handling(1, Vector2i(2, y));
				}
				//short castling
				else
				{
					board->event_handling(1, Vector2i(4, y));
					board->event_handling(1, Vector2i(6, y));
				}
			}
			//pawn
			else
			{
				if (notation.at(moveNr).size() == 2)
				{
					int x = translateCoordinate(notation.at(moveNr).at(0));
					int y = translateCoordinate(notation.at(moveNr).at(1));
					Vector2i figureCoordinates = searchFigure(PAWN, Vector2i(x, y));
					board->event_handling(1, figureCoordinates);
					board->event_handling(1, Vector2i(x, y));
				}
				else
				{
					int x = translateCoordinate(notation.at(moveNr).at(1));
					int y = translateCoordinate(notation.at(moveNr).at(2));
					Vector2i figureCoordinates = pawnException(notation.at(moveNr).at(0), y);
					board->event_handling(1, figureCoordinates);
					board->event_handling(1, Vector2i(x, y));
				}
			}
			moveNr++;
			whiteTurn = !whiteTurn;
		}
	}
}

Vector2i SceneMatchViewer::pawnException(char c, int y)
{
	int x = translateCoordinate(c);
	if (whiteTurn)
	{
		return Vector2i(x, y+1);
	}
	else
	{
		return Vector2i(x, y-1);
	}
}

void SceneMatchViewer::removeExtraLetters()
{
	for (int i = 0; i < notation.at(moveNr).size(); i++)
	{
		if (notation.at(moveNr).at(i) == 'x' ||
			notation.at(moveNr).at(i) == '+' ||
			notation.at(moveNr).at(i) == '#')
		{
			notation.at(moveNr).erase(i, 1);
		}
	}
}

Vector2i SceneMatchViewer::searchFigure(FigureType type, Vector2i destination)
{
	boardContent& content = board->get_content();
	for (int x = 0; x < 8; x++)
	{
		for (int y = 0; y < 8; y++)
		{
			if (content[y][x] != NULL)
			{
				if (content[y][x]->check_if_white() == whiteTurn)
				{
					if (content[y][x]->get_type() == type)
					{
						vector<Vector2i> destinations = content[y][x]->get_possible_destinations(Vector2i(x, y), content);
						for (int i = 0; i < destinations.size(); i++)
						{
							if (destinations.at(i) == destination)
							{
								return Vector2i(x, y);
							}
						}
					}
				}
			}
		}
	}
}

Vector2i SceneMatchViewer::searchFigure(FigureType type, Vector2i destination, char c)
{
	boardContent& content = board->get_content();
	if (isRow(c))
	{
		int y = translateCoordinate(c);
		for (int x = 0; x < 8; x++)
		{
			if (content[y][x] != NULL)
			{
				if (content[y][x]->check_if_white() == whiteTurn)
				{
					if (content[y][x]->get_type() == type)
					{
						vector<Vector2i> destinations = content[y][x]->get_possible_destinations(Vector2i(x, y), content);
						for (int i = 0; i < destinations.size(); i++)
						{
							if (destinations.at(i) == destination)
							{
								return Vector2i(x, y);
							}
						}
					}
				}
			}
		}
	}
	else
	{
		int x = translateCoordinate(c);
		for (int y = 0; y < 8; y++)
		{
			if (content[y][x] != NULL)
			{
				if (content[y][x]->check_if_white() == whiteTurn)
				{
					if (content[y][x]->get_type() == type)
					{
						vector<Vector2i> destinations = content[y][x]->get_possible_destinations(Vector2i(x, y), content);
						for (int i = 0; i < destinations.size(); i++)
						{
							if (destinations.at(i) == destination)
							{
								return Vector2i(x, y);
							}
						}
					}
				}
			}
		}
	}
}

int SceneMatchViewer::translateCoordinate(char c)
{
	for (int i = 0; i < 8; i++)
	{
		if (c == datax.at(i) || c == datay.at(i))
		{
			return i;
		}
	}
}

bool SceneMatchViewer::isRow(char c)
{
	for (int x = 0; x < datax.size(); x++)
	{
		if (c == datax.at(x))
		{
			return false;
		}
	}
	for (int y = 0; y < datay.size(); y++)
	{
		if (c == datax.at(y))
		{
			return true;
		}
	}
}

void SceneMatchViewer::readPGNFile()
{
	string line;
	ifstream myfile("C:/Users/ASUS/source/repos/realchess - Kopie/realchess/data.txt");
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			stringstream currentLine(line);
			vector<string> segList;
			string segment;

			while (getline(currentLine, segment, ' '))
			{
				segList.push_back(segment);
			}
			notation.push_back(segList.at(1));
			notation.push_back(segList.at(2));
		}
		myfile.close();
	}
}

void SceneMatchViewer::shutdown()
{

}