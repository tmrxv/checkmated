#pragma once
#include "scenechess.h"
#include "sceneselection.h"
#include "scenetrainer.h"
#include "scenemainmenu.h"
#include "board.h"
#include "trainer.h"
#include <SFML/Graphics.hpp>

using namespace sf;
enum SceneName
{
	MAIN_MENU, CHESS, TRAINING, SELECTION, MATCH_VIEWER, MANAGER
};
class GUI
{
private:
	gui::RectangleShape background_colored_lower;
	gui::RectangleShape background_colored_upper;
	gui::Sprite background;

	int scene = 0;
	Scene* currentScene;
	Board board;
	Trainer trainer;
	bool boardInteractable = false;
public:
	GUI();
	virtual ~GUI();

	//functions
	void draw(RenderWindow&);
	void event_handling(int, Vector2f);
	void changeColor(Color);
	void changeScene(SceneName scene);
	void hideBoard();
	void showBoard();
	void setBoardInteractable(bool);
};