#include "component.h"
#include <iostream>

Component::Component()
{
}

Component::~Component()
{
}

void Component::draw(RenderWindow& window, Vector2f offset, int from, int to)
{
	//error correction
	if (from < 0) from = 0;
	if (to > parts.size() - 1) to = (int)(parts.size() - 1);

	//draw objects within range
	for (int i = from; i <= to; i++)
	{
		parts.at(i)->draw(window, offset + position);
	}
}

void Component::push_back(GraphicalObject* object)
{
	parts.push_back(object);
}

void Component::draw(RenderWindow& window, Vector2f offset)
{
	draw(window, offset, 0, (int)(parts.size() - 1));
}

void Component::draw(RenderWindow& window)
{
	draw(window, Vector2f(0, 0), 0, (int)(parts.size() - 1));
}

void Component::draw(RenderWindow& window, int from, int to)
{
	draw(window, Vector2f(0, 0), from, to);
}


void Component::setAlphas()
{
	for (auto part : parts)
	{
		if (part->isEffectedByGlobalAlpha())
		{
			part->setAlpha(alpha);
		}
	}
}

void Component::update()
{
	do_fading();
	do_moving();
}