#include "movable.h"

Movable::Movable()
{
}

Movable::~Movable()
{
}

void Movable::do_moving()
{
	if (move_type == SLOWDOWN)
	{
		float way_to_goX = destination_position.x - position.x;
		float way_to_goY = destination_position.y - position.y;

		if (way_to_goX < 0)
		{
			way_to_goX = way_to_goX * -1;
		}
		if (way_to_goY < 0)
		{
			way_to_goY = way_to_goY * -1;
		}

		//movement
		if (way_to_goX <= 1)
		{
			position.x = destination_position.x;
		}
		else
		{
			position.x += speed_movement / 100 * (destination_position.x - position.x);
		}
		if (way_to_goY <= 1)
		{
			position.y = destination_position.y;
		}
		else
		{
			position.y += speed_movement / 100 * (destination_position.y - position.y);
		}
	}
	else if (move_type == ACCELERATE)
	{
		Vector2f previous_position = position;
		float dirX = (destination_position.x - position.x) > 0 ? 3 : -3;
		float dirY = (destination_position.y - position.y) > 0 ? 3 : -3;
		position.x += speed_movement / 25 * (position.x - start_position.x + dirX);
		position.y += speed_movement / 25 * (position.y - start_position.y + dirY);

		if (dirX > 0)
		{
			if (position.x > destination_position.x)
			{
				position.x = destination_position.x;
			}
		}
		else
		{
			if (position.x < destination_position.x)
			{
				position.x = destination_position.x;
			}
		}
		if (dirY > 0)
		{
			if (position.y > destination_position.y)
			{
				position.y = destination_position.y;
			}
		}
		else
		{
			if (position.y < destination_position.y)
			{
				position.y = destination_position.y;
			}
		}
	}
	//trigger
	if (position.x > destination_position.x-30 && position.x < destination_position.x + 30)
	{
		if (position.y > destination_position.y - 30 && position.y < destination_position.y + 30)
		{
			if (wait_for_trigger)
			{
				wait_for_trigger = false;
				trigger = true;
			}
		}
	}
}

void Movable::move_to(Vector2f destination, float speed_movement, MovementType move_type)
{
	this->move_type = move_type;
	this->destination_position = destination;
	this->speed_movement = speed_movement;
	this->start_position = position;
	trigger = false;
	wait_for_trigger = true;
}

void Movable::move_by(Vector2f distance, float speed_movement, MovementType move_type)
{
	this->move_type = move_type;
	this->destination_position += distance;
	this->speed_movement = speed_movement;
	this->start_position = position;
	trigger = false;
	wait_for_trigger = true;
}

bool Movable::is_triggered()
{
	if (trigger)
	{
		trigger = false;
		return true;
	}
	return false;
}

void Movable::setPosition(Vector2f position)
{
	this->position = position;
	this->destination_position = position;
}

Vector2f Movable::getPosition()
{
	return position;
}