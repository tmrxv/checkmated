#include "trainer.h"

Trainer::Trainer()
{
	font.loadFromFile("C:/Windows/Fonts/calibri.ttf");
	lectionPawn = new LectionPawn("Bauer");
	lectionBishop = new LectionBishop("L�ufer");
	lectionKnight = new LectionKnight("Springer");
	lectionCastle = new LectionCastle("Turm");
	lectionQueen = new LectionQueen("Dame");
	lectionKing = new LectionKing("K�nig");

	lections.push_back(lectionPawn);
	lections.push_back(lectionBishop);
	lections.push_back(lectionKnight);
	lections.push_back(lectionCastle);
	lections.push_back(lectionQueen);
	lections.push_back(lectionKing);

	//strategy1 = new Strategy("yes");
	//strategies.push_back(strategy1);
}

Trainer::~Trainer()
{
	//todo delete
	delete lectionPawn;
	delete lectionBishop;
	delete lectionKnight;
}

vector<Lection*> Trainer::get_lections()
{
	return lections;
}

vector<Lection*> Trainer::get_strategies()
{
	return strategies;
}

vector<gui::Text> Trainer::get_lection_names()
{
	for (int i = 0; i < lections.size(); i++)
	{
		gui::Text text = gui::Text(font, Color::White, 20);
		text.setString(lections.at(i)->get_name());
		lectionNames.push_back(text);
	}
	return lectionNames;
}

vector<gui::Text> Trainer::get_strategy_names()
{
	for (int i = 0; i < strategies.size(); i++)
	{
		gui::Text text = gui::Text(font, Color::White, 20);
		text.setString(strategies.at(i)->get_name());
		strategyNames.push_back(text);
	}
	return strategyNames;
}

void Trainer::set_selected_lection(Lection* selectedLection)
{
	this->selectedLection = selectedLection;
}

Lection* Trainer::get_selected_lection()
{
	return selectedLection;
}
