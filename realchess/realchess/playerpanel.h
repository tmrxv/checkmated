#pragma once
#include "container.h"
#include "rectangleshape.h"
#include "label.h"
#include "gui.h"
#include "board.h"
#include <chrono>

enum GameState
{
	PLAYING, PAUSED, RESET, SAVE, EXIT
};

class PlayerPanel : public Container
{
protected:
	GameState state = PLAYING;
	Label playerName;
	Label clock;
	gui::Sprite* nameLabel;
	gui::Sprite* watch;
	gui::Sprite* yes;
	gui::Sprite* no;
	gui::RectangleShape pane;
	gui::RectangleShape blackscreen;
	gui::Text pauseText;
	gui::Text pauseInfo;
	chrono::seconds second;
	chrono::minutes minute;
	chrono::hours hour;
	vector<FloatRect> buttons;
	vector<Label*> labels;
	Board* board;
	GUI* gui;
	int currentLabel = 0;
	bool firstPart = true;
	bool inputAllowed = true;
	bool notationFull = false;
public:
	PlayerPanel();
	PlayerPanel(Font&, bool, GUI*, Board*);
	//PlayerPanel &operator=(const PlayerPanel&);
	virtual ~PlayerPanel();

	//functions
	void drawMe(RenderWindow&);
	void addNotation(vector<string>*);
	void event_handling(int, Vector2f);
	void openPauseScreen();
	void closePauseScreen();
	void startTimer();
};