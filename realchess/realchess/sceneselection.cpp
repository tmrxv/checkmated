#include "sceneselection.h"

SceneSelection::SceneSelection()
{
}

SceneSelection::SceneSelection(GUI* gui, Board* board, Trainer* trainer) : Scene(gui)
{
	font.loadFromFile("C:/Windows/Fonts/calibri.ttf");

	this->board = board;
	this->trainer = trainer;
	lections = this->trainer->get_lections();
	lectionNames = this->trainer->get_lection_names();

	startup();
}

SceneSelection::~SceneSelection()
{
	//todo delete

}

void SceneSelection::draw(RenderWindow& window)
{
	for (int i = 0; i < lections.size(); i++)
	{
		lectionFields.at(i).draw(window);
		lectionNames.at(i).draw(window);
	}
}

void SceneSelection::event_handling(int left_mouse_click, Vector2f m_coordinates_fields)
{

	if (state == STARTING)
	{
		state = RUNNING;
	}
	else if (state == RUNNING)
	{
		if (left_mouse_click == 1)
		{
			for (int i = 0; i < selectButtons.size(); i++)
			{
				if (selectButtons.at(i).contains(m_coordinates_fields))
				{
					if (selectedLection != NULL)
					{
						if (selectedLection == lections.at(i))
						{
							trainer->set_selected_lection(selectedLection);
							if (selectedLection->check_if_content_is_std())
							{
								board->reset_game();
							}
							else
							{
								board->set_content(selectedLection->get_content());
							}
							board->set_textures();
							gui->changeScene(TRAINING);
							state = CLOSING;
							return;
						}
						else
						{
							selectedLection = NULL;
						}
					}
					selectedLection = lections.at(i);
				}
			}
		}
	}
	//update
	for (int i = 0; i < lections.size(); i++)
	{
		lectionFields.at(i).update();
		lectionNames.at(i).update();
	}

}

void SceneSelection::startup()
{
	rectLection.setFillColor(Color::Black);
	rectLection.setSize({ 200, 120 });

	float offset = 50;
	float x = 0;
	//blocks
	for (float i = 0; i < lections.size(); i++)
	{
		if ((x * (40 + rectLection.getSize().x)) == 960)
		{
			offset = rectLection.getSize().y + 2 * offset;
			x = 0;
		}
		else
		{
			x = i;
		}
		rectLection.setPosition({ (x * (40 + rectLection.getSize().x)), offset });
		lectionFields.push_back(rectLection);
		selectButton = FloatRect(Vector2f((x * (40 + rectLection.getSize().x)), offset), { 200, 120 });
		selectButtons.push_back(selectButton);
	}
	//names
	for (int i = 0; i < lections.size(); i++)
	{
		lectionNames.at(i).setPosition(Vector2f(lectionFields.at(i).getPosition()));
	}
	//animation
	for (int i = 0; i < lections.size(); i++)
	{
		lectionFields.at(i).move_by(Vector2f(60, 0), 12, SLOWDOWN);
		lectionNames.at(i).move_by(Vector2f(60, 0), 12, SLOWDOWN);
	}
}

void SceneSelection::shutdown()
{
}